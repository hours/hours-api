var assert = require("assert");

var request = require('supertest');

var helper = require('../Helper')

var express = require('express');

var shiftsApi = require('../../src/api/ShiftsApi.js');
var projectsApi = require('../../src/api/ProjectsApi.js');

describe('ProjectTests', function () {

    this.timeout(10000);

    var app = express();
    var mongodb = null;

    // Connect database
    before(function (done) {
        helper.connect(function (err, db) {
            if (err) throw err;
            mongodb = db;
            done();
        });
    });

    // Start server
    before(function (done) {

        var context = function (req, res, next) {

            req.user = req.headers.user;

            req.context = {
                organisation: req.headers.organisation
            };

            if (req.params.user) req.context.user = req.params.user;
            if (req.params.organisation) req.context.organisation = req.params.organisation;

            next();
        };

        app.use("/organisations/:organisation/users/:user", context, shiftsApi(mongodb));
        app.use("/organisations/:organisation", context, shiftsApi(mongodb));
        app.use("/", context, shiftsApi(mongodb));

        app.use("/organisations/:organisation", context, projectsApi(mongodb));

        done()

    });

    describe('GET /projects', function () {


        before(function (done) {
            helper.loadTestData("ProjectsFlowTest", mongodb, done);
        });

        it('should only return projects assigned to User A', function (done) {

            request(app)
                .get('/organisations/000000000000000000000000/projects')
                .set('user', "100000000000000000000000")
                .set('organisations', "000000000000000000000000")
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.length, 2);

                    assert.equal(res.body[0].name, 'Project A');
                    assert.equal(res.body[1].name, 'Project B');

                    done()
                });
        });

        it('should only return projects assigned to User B', function (done) {

            request(app)
                .get('/organisations/000000000000000000000000/projects')
                .set('user', "100000000000000000000001")
                .set('organisations', "000000000000000000000000")
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.length, 1);

                    assert.equal(res.body[0].name, 'Project A');

                    done()
                });
        });

        it('should only return projects assigned to User C', function (done) {

            request(app)
                .get('/organisations/000000000000000000000000/projects')
                .set('user', "100000000000000000000002")
                .set('organisations', "000000000000000000000000")
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.length, 3);

                    assert.equal(res.body[0].name, 'Project A');
                    assert.equal(res.body[1].name, 'Project B');
                    assert.equal(res.body[2].name, 'Project C');

                    done()
                });
        });

    });

    describe('GET /shifts', function () {


        before(function (done) {
            helper.loadTestData("ProjectsFlowTest", mongodb, done);
        });

        it('should only return shifts assigned to User A and Project', function (done) {

            request(app)
                .get('/organisations/000000000000000000000000/shifts')
                .set('user', "100000000000000000000000")
                .set('organisations', "000000000000000000000000")
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.length, 3);

                    assert.equal(res.body[0]._id, '200000000000000000000000');
                    assert.equal(res.body[0].user, '100000000000000000000000');

                    assert.equal(res.body[1]._id, '200000000000000000000001');
                    assert.equal(res.body[1].user, '100000000000000000000000');

                    assert.equal(res.body[2]._id, '200000000000000000000004');
                    assert.equal(res.body[2].user, '100000000000000000000002');

                    done()
                });
        });

    });

});