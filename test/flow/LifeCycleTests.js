var assert = require("assert");

var request = require('supertest');

var helper = require('../Helper')

var express = require('express');

var shiftsApi = require('../../src/api/ShiftsApi.js');


describe('ShiftLifeCycleTests', function () {

    this.timeout(10000);

    var app = express();
    var mongodb = null;
    
    // Connect database
    before(function (done) {
        helper.connect(function (err, db) {
            if (err) throw err;
            mongodb = db;
            done();
        });
    });

    // Start server
    before(function (done) {

        
        var context = function (req, res, next) {

            req.user = "100000000000000000000000";

            req.context = {
                organisation: "000000000000000000000000",
                user: "100000000000000000000000"
            };

            if (req.params.user) req.context.user = req.params.user;
            if (req.params.organisation) req.context.organisation = req.params.organisation;

            next();
        };

        app.use("/organisations/:organisation/users/:user", context, shiftsApi(mongodb));
        app.use("/organisations/:organisation", context, shiftsApi(mongodb));
        app.use("/", context, shiftsApi(mongodb));

        done()
        
    });

    describe('User is Owner of Organisation no workers', function () {


        before(function (done) {
            helper.loadTestData("OneOrganisation_UserOwner_NoWorkers", mongodb, done);
        });

        it('should add a shift', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.status, "approved");
                    assert.equal(res.body.user, "100000000000000000000000");
                    done()
                })
        });

        it('should add a shift when user is set in body', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "100000000000000000000000"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.status, "approved")
                    assert.equal(res.body.user, "100000000000000000000000");
                    done()
                })
        });

        it('should add a shift with status blocked', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "100000000000000000000000",
                status: "blocked"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.status, "blocked")
                    assert.equal(res.body.user, "100000000000000000000000");
                    done()
                })
        });
    });

    describe('User is Owner of Organisation One Worker', function () {


        before(function (done) {
            mongodb.dropDatabase();
            helper.loadTestData("OneOrganisation_UserOwner_OneWorkers", mongodb, done);
        });

        beforeEach(function (done) {
            mongodb.collection("shifts").drop(function(){
                done()
            });
        });

        it('should add a shift when user is null', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: null
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.status, "open");
                    assert.equal(res.body.user, null);
                    done()
                });
        });

        it('should add a shift for worker', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "100000000000000000000001"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.status, "assigned");
                    assert.equal(res.body.user, "100000000000000000000001");
                    done()
                });
        });

        it('should add a shift for worker with status blocked', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "100000000000000000000000",
                status: "blocked"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.status, "blocked")
                    assert.equal(res.body.user, "100000000000000000000000");
                    done()
                })
        });

        it('should not add a shift for non existing user ', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "xxxxxxxxxxxxxxxxxxxx"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .expect(4)
                .end(function (err, res) {
                    assert.equal(res.text, "SET_USER_HAS_NO_ROLE_IN_ORGANISATION")
                    done()
                });
        });

        it('should update a new shift with a user', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: null,
                status: "open"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200)
                    assert.equal(res.body.status, "open");
                    assert.equal(res.body.user, null);

                    shift.user = "100000000000000000000001"
                    request(app)
                        .put('/shifts/' + res.body._id)
                        .send(shift)
                        .end(function (err, res) {
                            assert.equal(res.status, 200)
                            assert.equal(res.body.status, "open");
                            assert.equal(res.body.user, "100000000000000000000001");
                            done()
                        });
                });
        });

        it('should remove a user from a shift', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "100000000000000000000001"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200)
                    assert.equal(res.body.status, "assigned");
                    assert.equal(res.body.user, "100000000000000000000001");

                    shift.user = null;
                    request(app)
                        .put('/shifts/' + res.body._id)
                        .send(shift)
                        .end(function (err, res) {
                            assert.equal(res.status, 200)
                            assert.equal(res.body.status, "open");
                            assert.equal(res.body.user, null);
                            done()
                        });
                });
        });

        it('should set category when create shift of user null', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: null,
                category: "TEST_CATEGORY"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200)
                    assert.equal(res.body.status, "open")
                    assert.equal(res.body.category, "TEST_CATEGORY")
                    done();
                });
        });

        it('should update category when user is null', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: null,
                category: "TEST_CATEGORY"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200)
                    assert.equal(res.body.status, "open")
                    assert.equal(res.body.category, "TEST_CATEGORY")

                    shift.category = "UPDATE_CATEGORY"
                    request(app)
                        .put('/shifts/' + res.body._id)
                        .send(shift)
                        .end(function (err, res) {
                            assert.equal(res.status, 200)
                            assert.equal(res.body.status, "open");
                            assert.equal(res.body.category, "UPDATE_CATEGORY")
                            done()
                        });
                });
        });


        it('should update category when user is worker', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "100000000000000000000001",
                category: "TEST_CATEGORY"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200)
                    assert.equal(res.body.status, "assigned")
                    assert.equal(res.body.category, "TEST_CATEGORY")

                    shift.category = "UPDATE_CATEGORY"
                    request(app)
                        .put('/shifts/' + res.body._id)
                        .send(shift)
                        .end(function (err, res) {
                            assert.equal(res.status, 200)
                            assert.equal(res.body.category, "UPDATE_CATEGORY")
                            done()
                        });
                });
        });


        it('should delete shift', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "100000000000000000000001",
                category: "TEST_CATEGORY"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.status, "assigned");
                    assert.equal(res.body.category, "TEST_CATEGORY");

                    shift.category = "UPDATE_CATEGORY";
                    request(app)
                        .delete('/shifts/' + res.body._id)
                        .end(function (err, res) {
                            assert.equal(res.status, 200);
                            mongodb.collection("shifts").find().toArray(function (err, res) {
                                assert.equal(res.length, 1);
                                assert.equal(res[0].status, 'removed');
                                done()
                            })

                        });
                });
        });

        it('should update to new when conext.user is shift.user and status is assigned', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "100000000000000000000001",
                category: "TEST_CATEGORY"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.status, "assigned");
                    assert.equal(res.body.category, "TEST_CATEGORY");

                    shift.category = "UPDATE_CATEGORY";
                    request(app)
                        .delete('/shifts/' + res.body._id)
                        .end(function (err, res) {
                            assert.equal(res.status, 200);
                            mongodb.collection("shifts").find().toArray(function (err, res) {
                                assert.equal(res.length, 1);
                                assert.equal(res[0].status, 'removed');
                                done()
                            })

                        });
                });
        });
    });


    describe('User is Worker of Organisation One workers', function () {


        before(function (done) {
            mongodb.dropDatabase();
            helper.loadTestData("OneOrganisation_UserWorker_OneWorkers", mongodb, done);
        });

        beforeEach(function (done) {
            mongodb.collection("shifts").drop(function(){
                done()
            });
        });

        it('should add a shift', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",

            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.status, "pending");
                    assert.equal(res.body.user, "100000000000000000000000");
                    done()
                })
        });

        it('should add a shift with status blocked', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                status: "blocked"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .expect(200)
                .end(function (err, res) {
                    assert.equal(res.body.status, "blocked")
                    assert.equal(res.body.user, "100000000000000000000000");
                    done()
                })
        });

        it('should not add a shift with status approved', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                status: "approved"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 403);
                    assert.equal(res.text, "NOT_ALLOWED_TO_SET_STATUS_APPROVED")
                    done()
                })
        });


        it('should not be possible to add shift for other user', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                user: "100000000000000000000001"
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .end(function (err, res) {
                    assert.equal(res.status, 403);
                    assert.equal(res.text, "NOT_ALLOWED_TO_ADD_SHIFT_FOR_OTHER_USER")
                    done();
                })
        });
    });


});