var fs = require("fs");
var async = require("async");
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;

module.exports = {

    connect: function (callback) {
        if (!process.env.MONGO_URL)
            process.env.MONGO_URL = 'mongodb://localhost:27017/hours-regression';

        MongoClient.connect(process.env.MONGO_URL, callback);
    },

    context: function (user, organisation, context) {


        return function (req, res, next) {

            req.user = user;

            req.context = {};

            if (context === null || context === true)
                req.context.user = user;

            if (organisation)
                req.context.organisation = organisation;

            if (req.params.user) req.context.user = req.params.user;
            if (req.params.organisation) req.context.organisation = req.params.organisation;

            next();

        }
    },

    loadTestData: function LoadTestData(file, mongodb, callback) {

        mongodb.dropDatabase();

        file = __dirname + '/../data/' + file + '.json';
        fs.readFile(file, function (err, data) {
            if (err) return callback(err);

            var obj = JSON.parse(data);

            var exec = [];
            Object.keys(obj).forEach(function (key) {
                exec.push(function (done) {
                    var collection = mongodb.collection(key);

                    var result = obj[key].map(function (val) {
                        if (val._id) val._id = ObjectID(val._id);
                        if (val.from) val.from = new Date(val.from);
                        if (val.to) val.to = new Date(val.to);
                        if (val.date) val.date = new Date(val.date);

                        return val;
                    });

                    collection.insert(result, function (err) {
                        done();
                    });
                })
            });

            async.parallel(exec, callback);

        });
    }

};