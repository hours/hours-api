var express = require('express');

var helper = require('../Helper');

var tokenApi = require('../../src/api/TokenApi.js');

var request = require('supertest');
var expect = require('chai').expect;

describe('TokenApi', function () {

    var router = express.Router();
    var mongodb = null;

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });

    before(function (done) {
        var config = {
            jwtTokenSecret: 'SECRET'
        }
        router.use(helper.context("100000000000000000000000", "000000000000000000000000"))
        router.use(tokenApi(mongodb, config));
        done();

    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Token_API", mongodb, done);
    });

    describe('GET /token', function () {

        it('should return the authenticated token from the database', function (done) {

            request(router)
                .get('/token')
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.token).to.contains('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9');
                    expect(res.body.user).to.equals('100000000000000000000000');
                    expect(res.body.organisation).to.equals('000000000000000000000000');
                    done();
                });


        });
    });
});
