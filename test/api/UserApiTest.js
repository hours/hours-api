var express = require('express');

var helper = require('../Helper');

var usersApi = require('../../src/api/UsersApi.js');

var request = require('supertest');
var expect = require('chai').expect;


describe('UserApi', function () {

    var app = express();
    var mongodb = null;

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });


    before(function (done) {
        app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
        app.use(usersApi(mongodb));
        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Users_API", mongodb, done);
    });

    describe('GET /users/me', function () {

        it('should return the authenticated user from the database', function (done) {

            request(app)
                .get('/users/me')
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.name).to.equal('Willem Veelenturf');
                    expect(res.body.username).to.equal('willem.veelenturf@gmail.com');
                    expect(res.body.password).to.be.undefined;
                    done();
                });
        });
    });
});
