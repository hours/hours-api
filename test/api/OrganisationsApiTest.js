var express = require('express');
var bodyParser = require('body-parser');

var helper = require('../Helper')

var organisationsApi = require('../../src/api/OrganisationsApi.js');

var request = require('supertest');
var expect = require('chai').expect

describe('OrganisationsApi', function () {

    var mongodb = null;
    var app = express();

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });

    before(function (done) {
        app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
        app.use(organisationsApi(mongodb));
        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Ogranisations_API", mongodb, done);
    });

    describe('GET /organisations', function () {
        it('should return all the organisations from the database the user', function (done) {
            request(app)
                .get('/organisations')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body[0].name).to.equal('Venster 33');
                    done();
                });
        })
    });

    describe('GET /organisations/:id', function () {
        it('should return one organisation from the database by id', function (done) {
            request(app)
                .get('/organisations/000000000000000000000000')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.name).to.equal('Venster 33');
                    done();
                });
        })
        it('should not return organisation when user has no role in organistaion', function (done) {
            request(app)
                .get('/organisations/000000000000000000000001')
                .expect(200)
                .end(function (err, res) {
                    expect(res.text).to.equal("USER_NOT_FOUND_IN_ORGANISATION");
                    done();
                });
        })
    })

    describe('POST /organisations', function () {
        var organisation = {
            name: "Test",
            description: "Cafe/Restaurant",
            logo: 'Logo123',
            settings: {
                multipleshifts: true,
                pause: true,
                availability: true,
                swap: true
            }
        };
        it('should create one organisations into the database', function (done) {
            request(app)
                .post('/organisations')
                .send(organisation)
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.name).to.equal('Test');
                    expect(res.body.description).to.equal('Cafe/Restaurant');
                    expect(res.body.logo).to.equal('Logo123');

                    expect(res.body.settings.multipleshifts).to.equal(true);
                    expect(res.body.settings.availability).to.equal(true);
                    expect(res.body.settings.swap).to.equal(true);

                    done();
                });
        })
    })

    describe('PUT /organisations/:id', function () {
        var organisation = {
            "name": "Test",
            "description": "Cafe/Restaurant",
            settings: {
                "multipleshifts": false,
                "pause": false
            }
        };
        it('should update one organisations into the database', function (done) {
            request(app)
                .put('/organisations/000000000000000000000002')
                .send(organisation)
                .expect(200)
                .end(function (err, res) {
                    expect(res.body._id).to.equal('000000000000000000000002');
                    expect(res.body.name).to.equal('Test');
                    expect(res.body.features[0]).to.equal('feature_1');
                    done();
                });
        })
        it('should not update when not owner of organisation', function (done) {
            request(app)
                .put('/organisations/000000000000000000000003')
                .send(organisation)
                .expect(500)
                .end(function (err, res) {
                    expect(res.text).to.equal('NOT_ALLOWED_TO_UPDATE_ORGANISATION_IN_THIS_ROLE');
                    done();
                });
        });
        it('should not update when not owner not active', function (done) {
            request(app)
                .put('/organisations/000000000000000000000004')
                .send(organisation)
                .expect(500)
                .end(function (err, res) {
                    expect(res.text).to.equal('USER_NOT_FOUND_IN_ORGANISATION');
                    done();
                });
        })
    })

    describe('POST /organisations/:id/join', function () {
        it('share organisation with worker', function (done) {
            request(app)
                .post('/organisations/000000000000000000000005/join')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.users.length).to.equal(2);
                    done();
                });
        })
        it('join organisation with worker and set to active when alread exists', function (done) {
            request(app)
                .post('/organisations/000000000000000000000006/join')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.users.length).to.equal(2);
                    expect(res.body.users[1].active).to.equal(true);
                    done();
                });
        })
    })

    describe('DELETE /organisations/:id', function () {
        it('delete user form organisation', function (done) {
            request(app)
                .delete('/organisations/000000000000000000000007')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.users[0].active).to.equal(false);
                    done();
                });
        })
    })

    describe('GET /organisations/:id/users', function () {
        it('get all users form organisation when user role is owner', function (done) {
            request(app)
                .get('/organisations/000000000000000000000000/users')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body[0].name).to.equal("Willem Veelenturf");
                    expect(res.body[1].name).to.equal("Piet Veelenturf");
                    done();
                });
        })
        it('get aprover users form organisation when user role is approver', function (done) {
            request(app)
                .get('/organisations/000000000000000000000008/users')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.length).to.equal(2);
                    expect(res.body[0].name).to.equal("Willem Veelenturf");
                    expect(res.body[1].name).to.equal("Klaas Veelenturf");
                    done();
                });
        })
        it('get user form organisation when user role is worker', function (done) {
            request(app)
                .get('/organisations/000000000000000000000003/users')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.length).to.equal(1);
                    expect(res.body[0].name).to.equal("Willem Veelenturf");
                    done();
                });
        })
    })

    describe('POST /organisations/:id/users', function () {
            var empl = {
                "_id": "100000000000000000000099",
            }


            var role='worker';

            it('should create one user in organisation only if role is owner', function (done) {
                request(app)
                    .post('/organisations/000000000000000000000000/users/')
                    .send(empl)
                    .expect(200)
                    .end(function (err, res) {
                        console.log(res);
                        expect(res.body.users.length).to.equal(3);
                        done();
                    });
            })
        });



    describe('PUT /organisations/:organisationId/users/:userId', function () {
        it('should update user in organisation', function (done) {

            var user = {
                "id": "100000000000000000000001",
                "role": "worker",
                "ref": "testref",
                "active": true
            }

            request(app)
                .put('/organisations/000000000000000000000000/users/100000000000000000000001')
                .send(user)
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.users.length).to.equal(2);
                    expect(res.body.users[1].id).to.equal("100000000000000000000001");
                    expect(res.body.users[1].ref).to.equal("testref");
                    expect(res.body.users[1].active).to.equal(true);
                    done();
                });
        })
        it('should update user in organisation and update role to approver', function (done) {

            var user = {
                "id": "100000000000000000000001",
                "approver": [
                    {
                        "id": "100000000000000000000002",
                        "active": true
                    }
                ]
            }

            request(app)
                .put('/organisations/000000000000000000000000/users/100000000000000000000001')
                .send(user)
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.users[1].id).to.equal("100000000000000000000001");
                    expect(res.body.users[1].role).to.equal("approver");
                    expect(res.body.users[1].active).to.equal(true);
                    done();
                });
        })
    })

    describe('DELETE /organisations/:organisationId/users/:userId', function () {
        it('should delete user in organisation', function (done) {
            request(app)
                .delete('/organisations/000000000000000000000000/users/100000000000000000000001')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.users[1].id).to.equal("100000000000000000000001");
                    expect(res.body.users[1].active).to.equal(false);
                    done();
                });
        })
    });
});
