var express = require('express');

var helper = require('../Helper');

var categoriesApi = require('../../src/api/CategoriesApi.js');

var request = require('supertest');
var expect = require('chai').expect

describe('CategoriesApi', function () {

    var app = express();
    var mongodb = null;

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });

    before(function (done) {
        app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
        app.use(categoriesApi(mongodb));
        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Categories_API", mongodb, done);
    });

    describe('GET /categories', function () {

        it('should return all the categories from the context organisation', function (done) {
            request(app)
                .get('/categories')
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.length).to.equal(1);
                    expect(res.body[0].name).to.equal('Category 1234');
                    expect(res.body[0].code).to.equal('1234');
                    done();
                });
        })
    })

    describe('POST /categories', function () {

        var category = {
            name: "test 123",
            code: "123"
        }
        it('should creat a new category for the context organisation', function (done) {
            request(app)
                .post('/categories')
                .send(category)
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.name).to.equal('test 123');
                    expect(res.body.code).to.equal('123');
                    expect(res.body.active).to.equal(true);
                    done();
                });
        })
    })

    describe('PUT /categories/:id', function () {

        var category = {
            name: "test 789",
            code: "789"
        }
        it('should creat a new category for the context organisation', function (done) {
            request(app)
                .put('/categories/200000000000000000000000')
                .send(category)
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.name).to.equal('test 789');
                    expect(res.body.code).to.equal('789');
                    expect(res.body.active).to.equal(true);
                    done();
                });
        })
    })

    describe('DELETE /categories/:id', function () {

        it('should creat a new category for the context organisation', function (done) {
            request(app)
                .delete('/categories/200000000000000000000000')
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.name).to.equal('Category 1234');
                    expect(res.body.code).to.equal('1234');
                    expect(res.body.active).to.equal(false);
                    done();
                });
        })
    })

})
