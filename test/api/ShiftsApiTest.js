var express = require('express');

var helper = require('../Helper');

var shiftsApi = require('../../src/api/ShiftsApi.js');

var ObjectID = require('mongodb').ObjectID;

var request = require('supertest');

var expect = require('chai').expect;
var moment = require('moment');

var app = express();

describe('ShiftsApi', function () {

    var app = express();
    var mongodb = null;

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });

    before(function (done) {

        var context = helper.context("100000000000000000000000");

        app.use("/organisations/:organisation/users/:user", context, shiftsApi(mongodb));
        app.use("/organisations/:organisation", context, shiftsApi(mongodb));
        app.use("/", context, shiftsApi(mongodb));

        done();

    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Shifts_API", mongodb, done);
    });

    describe('GET /shifts', function () {

        it('should return all the shifts for the organisation when user is owner', function (done) {
            request(app)
                .get('/organisations/000000000000000000000000/shifts')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.length).to.equal(3);
                    done();
                });
        });

        it('should return the shifts for the organisation where user is user when use is not owner', function (done) {

            request(app)
                .get('/organisations/000000000000000000000001/shifts')
                .expect(200)
                .end(function (err, res) {
                    expect(res.body.length).to.equal(1);
                    done();
                });

        })

    });

    describe('GET shifts/count', function (done) {

        it('should return the amount of shifts from the database matching the context', function (done) {
            request(app)
                .get('/organisations/000000000000000000000000/shifts/count')
                .expect(200)
                .end(function (err, res) {
                    expect(res.text).to.equal("3");
                    done();
                });

        })

        it('should return the amount of shifts from the database matching the context with status pending', function (done) {
            request(app)
                .get('/organisations/000000000000000000000000/shifts/count')
                .query({
                    status: 'pending'
                })
                .expect(200)
                .end(function (err, res) {
                    expect(res.text).to.equal("2");
                    done();
                });

        })
    });

    describe('POST /shifts', function () {

        it('should add one shift to the database full object', function (done) {

            var shift = {
                from: "2015-06-08T05:00:00.000Z",
                to: "2015-06-08T12:00:00.000Z",
                category: "category",
                organisation: "000000000000000000000001",
                pause: null
            };

            request(app)
                .post('/shifts')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal("100000000000000000000000");
                        expect(data.organisation).to.equal("000000000000000000000001");
                        expect(data.status).to.equal("pending");
                        expect(data.category).to.equal("category");
                        done();
                    })
                });
        })

        it('should add one shift to the database organisation context', function (done) {

            var shift = {
                "from": "2015-06-08T05:00:00.000Z",
                "to": "2015-06-08T12:00:00.000Z",
                "category": "category",
                "pause": null
            };

            request(app)
                .post('/organisations/000000000000000000000001/shifts')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal("100000000000000000000000");
                        expect(data.organisation).to.equal("000000000000000000000001");
                        expect(data.status).to.equal("pending");
                        expect(data.category).to.equal("category");
                        done();
                    })
                });
        })

        it('should add one shift to the database when role is owner status must be approved', function (done) {

            var shift = {
                "from": "2015-06-08T05:00:00.000Z",
                "to": "2015-06-08T12:00:00.000Z",
                "pause": null
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal("100000000000000000000000");
                        expect(data.organisation).to.equal("000000000000000000000000");
                        expect(data.status).to.equal("approved");
                        done();
                    })

                });

        })

        it('should add one shift to the database when role is owner and user is null then status must be open', function (done) {

            var shift = {
                "user": null,
                "from": "2015-06-08T05:00:00.000Z",
                "to": "2015-06-08T12:00:00.000Z",
                "pause": null
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal(null);
                        expect(data.organisation).to.equal("000000000000000000000000");
                        expect(data.status).to.equal("open");
                        done();
                    })

                });

        })

        it('should add one shift to the database when role is owner and user is an employee then status must be assigned', function (done) {

            var shift = {
                "user": "100000000000000000000001",
                "from": "2015-06-08T05:00:00.000Z",
                "to": "2015-06-08T12:00:00.000Z",
                "pause": null
            };

            request(app)
                .post('/organisations/000000000000000000000000/shifts')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal("100000000000000000000001");
                        expect(data.organisation).to.equal("000000000000000000000000");
                        expect(data.status).to.equal("assigned");
                        done();
                    })

                });

        })

        it('should add one shift to the database for an other user', function (done) {

            var shift = {
                "from": "2015-06-08T05:00:00.000Z",
                "to": "2015-06-08T12:00:00.000Z",
                "pause": null
            };

            request(app)
                .post('/organisations/000000000000000000000000/users/100000000000000000000001/shifts')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal("100000000000000000000001");
                        expect(data.organisation).to.equal("000000000000000000000000");
                        expect(data.status).to.equal("assigned");
                        done();
                    })
                });
        })

        it('should add no shift to the database when user has no role in the organisation', function (done) {

            var shift = {
                "from": "2015-06-08T05:00:00.000Z",
                "to": "2015-06-08T12:00:00.000Z",
                "pause": null
            };

            request(app)
                .post('/organisations/000000000000000000000000/users/xxxxxxxxxxxxxxxxxxxxxxxx/shifts')
                .send(shift)
                .expect(500)
                .end(function (err, res) {
                    var collection = mongodb.collection('shifts');
                    collection.find().toArray(function (err, data) {
                        expect(data.length).to.equal(4);
                        done();
                    })
                });
        })
    })


    describe('POST /shifts/:id/approve', function (done) {

        it('should change the status of the shift from pending to approved', function (done) {
            request(app)
                .post('/shifts/200000000000000000000000/approve')
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.find().toArray(function (err, data) {
                        expect(data.length).to.equal(4);
                        expect(data[0].user).to.equal("100000000000000000000000");
                        expect(data[0].organisation).to.equal("000000000000000000000000");
                        expect(data[0].status).to.equal("approved");
                        done();
                    })
                });
        })
    })

    describe('POST /shifts/:id/reject', function (done) {

        it('should change the status of the shift from pending to rejected', function (done) {
            request(app)
                .post('/shifts/200000000000000000000000/reject')
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.find().toArray(function (err, data) {
                        expect(data.length).to.equal(4);
                        expect(data[0].user).to.equal("100000000000000000000000");
                        expect(data[0].organisation).to.equal("000000000000000000000000");
                        expect(data[0].status).to.equal("rejected");
                        done();
                    })
                });
        })
    })

    describe('GET /shifts/:id', function (done) {

        it('should return one shift from the database', function (done) {
            request(app)
                .get('/shifts/200000000000000000000001')
                .expect(200)
                .end(function (err, res) {

                    expect(res.body._id).to.equal("200000000000000000000001");
                    done();
                });

        })
    })
    describe('PUT /shifts/:id', function (done) {

        var shift = {
            from: "2016-06-08T05:00:00.000Z",
            to: "2016-06-08T12:00:00.000Z",
            pause: 30,
            status: "approved"
        };

        it('should update one shift to the database and est status to approved when role is owner', function (done) {
            request(app)
                .put('/shifts/200000000000000000000001')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal("100000000000000000000000");
                        expect(data.organisation).to.equal("000000000000000000000000");
                        expect(data.pause).to.equal(30);
                        expect(data.status).to.equal("approved");
                        done();
                    })
                });
        })

        it('should update one shift to the database and set status to pending when role is worker 1', function (done) {

            var shift = {
                from: "2016-06-08T05:00:00.000Z",
                to: "2016-06-05T12:00:00.000Z",
                pause: 30,
                status: "approved"
            };

            request(app)
                .put('/shifts/200000000000000000000002')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal("100000000000000000000000");
                        expect(data.organisation).to.equal("000000000000000000000001");
                        expect(data.pause).to.equal(30);
                        expect(moment(data.to).utc().format()).to.equal("2016-06-08T12:00:00Z");
                        done();
                    })
                });
        });

        it('should update one shift to the database and set status to pending when role is worker 2', function (done) {

            var shift = {
                from: "2016-06-08T05:00:00.000Z",
                to: "2016-06-08T04:00:00.000Z",
                pause: 30,
                status: "approved"
            };

            request(app)
                .put('/shifts/200000000000000000000002')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal("100000000000000000000000");
                        expect(data.organisation).to.equal("000000000000000000000001");
                        expect(data.pause).to.equal(30);
                        expect(moment(data.to).utc().format()).to.equal("2016-06-09T04:00:00Z");
                        done();
                    })
                });
        });

        it('should update one shift to the database and correct to date', function (done) {

            request(app)
                .put('/shifts/200000000000000000000002')
                .send(shift)
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.findOne(ObjectID(res.body._id), function (err, data) {
                        expect(data.user).to.equal("100000000000000000000000");
                        expect(data.organisation).to.equal("000000000000000000000001");
                        expect(data.pause).to.equal(30);
                        expect(data.status).to.equal("pending");
                        done();
                    })
                });
        })
    })

    describe('DELETE /shifts/:id', function (done) {

        it('should remove one shift from the database', function (done) {

            request(app)
                .delete('/shifts/200000000000000000000001')
                .expect(200)
                .end(function (err, res) {

                    var collection = mongodb.collection('shifts');
                    collection.find({"_id": new ObjectID('200000000000000000000001')}).toArray(function (err, data) {
                        expect(data.length).to.equal(1);
                        expect(data[0].status).to.equal('removed');
                        done();
                    })
                });
        })

        it('should not allow when context.user is shift.user', function (done) {

            request(app)
                .delete('/shifts/200000000000000000000002')
                .expect(403)
                .end(function (err, res) {

                    done();
                });
        })

        it('should return error when delete non excisting shift', function (done) {

            request(app)
                .delete('/shifts/200000000000000000000009')
                .expect(200)
                .end(function (err, res) {
                    expect(res.text).to.equal('SHIFT_NOT_FOUND_IN_DATABASE')
                    var collection = mongodb.collection('shifts');
                    collection.find().toArray(function (err, data) {
                        expect(data.length).to.equal(4);
                        done();
                    })
                });
        })
    })
})
