var express = require('express');

var helper = require('../Helper');

var synchronizationApi = require('../../src/api/SynchronizationsApi.js');

var request = require('supertest');
var expect = require('chai').expect;

describe('SynchronizationsApi', function () {

    var mongodb = null;
    var app = express();

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });

    before(function (done) {
        app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
        app.use(synchronizationApi(mongodb));
        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Synchronizations_API", mongodb, done);
    });

    describe('GET /synchronizations', function () {

        it('should return list of synchronization for the organisation context', function (done) {

            request(app)
                .get('/synchronizations')
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.length).to.equal(2);
                    expect(res.body[0].code).to.equal('GOOGLE_DOCS');
                    expect(res.body[1].code).to.equal('NMBRS');
                    done();
                });
        });

    });

    describe('POST /synchronization', function () {

        it('should create synchronization', function (done) {

            var document = {
                name: 'New',
                code: 'NEW'
            };

            request(app)
                .post('/synchronizations')
                .send(document)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.name).to.equal('New');
                    expect(res.body.code).to.equal('NEW');
                    done();
                });
        });

    });
});
