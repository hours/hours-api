var express = require('express');

var helper = require('../Helper');

var transactionsApi = require('../../src/api/TransactionsApi.js');

var request = require('supertest');
var expect = require('chai').expect;


describe('TransactionsApi', function () {

    var app = express();
    var mongodb = null;

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });


    before(function (done) {
        app.use(helper.context("100000000000000000000000", "000000000000000000000000"));
        app.use(transactionsApi(mongodb));
        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Transactions_API", mongodb, done);
    });

    describe('GET /transactions', function () {

        it('should return list of all transactions', function (done) {

            request(app)
                .get('/transactions')
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body.length).to.equal(2);
                    expect(res.body[0].description).to.equal('Add credits');
                    expect(res.body[1].description).to.equal('Generate report: Hello - June 2015');
                    done();
                });
        });
    });

    describe('GET /transactions/balance', function () {

        it('should return total balace', function (done) {

            request(app)
                .get('/transactions/balance')
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.text).to.equal("45");
                    done();
                });
        });
    });
});
