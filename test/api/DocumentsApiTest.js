var express = require('express');

var helper = require('../Helper');

var documentsApi = require('../../src/api/DocumentsApi.js');

var request = require('supertest');
var expect = require('chai').expect;

describe('DocumentsApi', function () {

    var mongodb = null;
    var app = express();

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });

    before(function (done) {
        app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
        app.use(documentsApi(mongodb));
        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Documents_API", mongodb, done);
    });

    describe('GET /documents', function () {

        it('should return list of documents for the organisation context', function (done) {

            request(app)
                .get('/documents')
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.length).to.equal(2);
                    expect(res.body[0].some).to.equal('value1');
                    done();
                });
        });

        it('should return list of documents for the organisation context query by type CASH_STATE', function (done) {

            request(app)
                .get('/documents?type=CASH_STATE')
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.length).to.equal(1);
                    expect(res.body[0].some).to.equal('value1');
                    done();
                });
        });

    });

    describe('POST /documents', function () {

        it('should create document of type CASH_STATE ', function (done) {

            var document = {
                type: 'CASH_STATE',
                some: 'value'
            };

            request(app)
                .post('/documents')
                .send(document)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.some).to.equal('value');
                    done();
                });
        });

        it('should create document of type COMPLAINT ', function (done) {

            var document = {
                type: 'COMPLAINT',
                some: 'value'
            };

            request(app)
                .post('/documents')
                .send(document)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.some).to.equal('value');
                    done();
                });
        });

        it('should not create document of type UNKNOWN ', function (done) {

            var document = {
                type: 'UNKNOWN',
                some: 'value'
            };

            request(app)
                .post('/documents')
                .send(document)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(403);
                    expect(res.text).to.equal('TYPE_OF_DOCUMENT_DOES_NOT_EXIST');
                    done();
                });
        });

    });
});
