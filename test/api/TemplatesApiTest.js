var express = require('express');

var helper = require('../Helper');

var templatesApi = require('../../src/api/TemplatesApi.js');

var request = require('supertest');
var expect = require('chai').expect


describe('TemplatesApi', function () {

    var app = express();
    var mongodb = null;

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });


    before(function (done) {
        app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
        app.use(templatesApi(mongodb));
        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Templates_API", mongodb, done);
    });

    describe('GET /templates', function () {

        it('should return all the templates from the context organisation', function (done) {
            request(app)
                .get('/templates')
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.length).to.equal(2);
                    expect(res.body[0].type).to.equal('TYPE');
                    expect(res.body[1].type).to.equal('TYPE');
                    done();
                });
        })
    })

    describe('POST /templates', function () {

        var template = {
            organisation: "000000000000000000000000",
            type: "TYPE",
            shifts: []
        };
        it('should creat a new template for the context organisation', function (done) {
            request(app)
                .post('/templates')
                .send(template)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.type).to.equal('TYPE');
                    done();
                });
        })
    });

    describe('PUT /templates/:id', function () {

        var template = {
            organisation: "000000000000000000000000",
            type: "TYPE!",
            shifts: []
        };
        it('should creat a new template for the context organisation', function (done) {
            request(app)
                .put('/templates/200000000000000000000000')
                .send(template)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.type).to.equal('TYPE!');
                    done();
                });
        })
    });

});
