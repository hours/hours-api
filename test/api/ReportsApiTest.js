var express = require('express');

var helper = require('../Helper');

var reportsApi = require('../../src/api/ReportsApi.js');

var ObjectID = require('mongodb').ObjectID;

var async = require('async');
var request = require('supertest');
var expect = require('chai').expect

describe('ReportsApi', function () {

    var mongodb = null;
    var app = express();


    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });

    before(function (done) {
        app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
        app.use(reportsApi(mongodb));
        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Reports_API", mongodb, done);
    });

    describe('POST /reports', function () {

        var report = {
            "year": 2015,
            "month": 6,
            "organisation": "000000000000000000000000"
        };

        it('should generate the report for one month based on the shifts in the database', function (done) {
            request(app)
                .post('/reports')
                .send(report)
                .expect(200)
                .end(function (err, res) {
                    var report = res.body;
                    var collection = mongodb.collection('reports');
                    collection.findOne(ObjectID(report._id), function (err, data) {
                        expect(data.shifts.length).to.equal(4);
                        expect(data.users.length).to.equal(2);
                        expect(data.projects.length).to.equal(1);
                        done();
                    });
                });

        })
    })

    describe('GET /reports', function (done) {

        var id = null;

        beforeEach(function (done) {

            var report = {
                "year": 2015,
                "month": 6,
                "organisation": "000000000000000000000000"
            };

            request(app)
                .post('/reports')
                .send(report)
                .expect(200)
                .end(function (err, res) {
                    var collection = mongodb.collection('reports');
                    id = res.body._id;
                    collection.findOne(ObjectID(id), function (err, data) {
                        expect(data.shifts.length).to.equal(4);
                        expect(data.users.length).to.equal(2);
                        expect(data.projects.length).to.equal(1);
                        done();
                    });
                });
        });


        it('should return a report', function (done) {
            request(app)
                .get('/reports/' + id)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    var report = res.body;
                    done();
                });
        });

        it('should return a report', function (done) {
            request(app)
                .get('/reports/' + id)
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    var report = res.body;
                    done();
                });
        });

        it('should return a report in csv', function (done) {

            request(app)
                .get('/reports/' + id + '/csv')
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    var report = res.text;
                    done();
                });
        });

        it('should return a report in xlsx', function (done) {

            request(app)
                .get('/reports/' + id + '/xlsx')
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    var report = res.text;
                    done();
                });
        });
    });

})
;
