var express = require('express');

var helper = require('../Helper');

var devicesApi = require('../../src/api/DevicesApi.js');

var request = require('supertest');
var expect = require('chai').expect;

describe('devicesApi', function () {

    var mongodb = null;
    var app = express();

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });

    before(function (done) {
        app.use(helper.context("100000000000000000000000", "000000000000000000000000", true))
        app.use(devicesApi(mongodb));
        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Devices_API", mongodb, done);
    });

    describe('GET /devices', function () {

        it('should return list of devices', function (done) {

            request(app)
                .get('/devices')
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.status).to.equal(200);
                    expect(res.body.length).to.equal(1);
                    done();
                });
        });


    });

    describe('POST /devices', function () {



        it('should create device ', function (done) {

            var device = {
                uuid: '0000-00000-0000-0000'
            };

            request(app)
                .post('/devices')
                .send(device)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body._id).to.exist;
                    expect(res.body.uuid).to.be.equal('0000-00000-0000-0000');
                    done();
                });
        });

        it('should create device and update', function (done) {

            var device = {
                uuid: '0000-00000-0000-0000'
            };

            var deviceId;

            request(app)
                .post('/devices')
                .send(device)
                .expect(200)
                .end(function (err, res) {
                    if (err) return done(err);
                    expect(res.body._id).to.exist;
                    expect(res.body.uuid).to.be.equal('0000-00000-0000-0000');
                    deviceId = res.body._id
                    request(app)
                        .post('/devices')
                        .send(device)
                        .expect(200)
                        .end(function (err, res) {
                            if (err) return done(err);
                            expect(res.body._id).to.be.equal(deviceId);
                            expect(res.body.uuid).to.be.equal('0000-00000-0000-0000');
                            done();
                        });
                });


        });

    });
});
