var express = require('express');

var helper = require('../Helper');

var projectsApi = require('../../src/api/ProjectsApi.js');

var request = require('supertest');
var expect = require('chai').expect

describe('ProjectsApi', function () {


    var mongodb = null;

    before(function (done) {
        helper.connect(function (err, db) {
            if (err) return done(err);
            mongodb = db;
            done();
        });
    });

    before(function (done) {

        done();
    });

    beforeEach(function (done) {
        helper.loadTestData("Test_Projects_API", mongodb, done);
    });

    describe('GET /projects', function () {

        it('should return all the projects from the context organisation', function (done) {
            var app = express();
            app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
            app.use(projectsApi(mongodb));

            request(app)
                .get('/projects')
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.length).to.equal(2);
                    expect(res.body[0].name).to.equal('Project 1234');
                    expect(res.body[0].code).to.equal('1234');
                    expect(res.body[1].name).to.equal('Project 5678');
                    expect(res.body[1].code).to.equal('5678');
                    done();
                });
        });

        it('should return all the projects from the context organisation', function (done) {

            var app = express();
            app.use(helper.context("100000000000000000000001", "000000000000000000000000"))
            app.use(projectsApi(mongodb));

            request(app)
                .get('/projects')
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.length).to.equal(1);
                    expect(res.body[0].name).to.equal('Project 1234');
                    expect(res.body[0].code).to.equal('1234');
                    done();
                });
        })
    });

    describe('POST /projects', function () {

        var project = {
            name: "test 123",
            code: "123"
        };
        it('should creat a new project for the context organisation', function (done) {
            var app = express();
            app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
            app.use(projectsApi(mongodb));

            request(app)
                .post('/projects')
                .send(project)
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.name).to.equal('test 123');
                    expect(res.body.code).to.equal('123');
                    expect(res.body.active).to.equal(true);
                    done();
                });
        })
    });

    describe('PUT /projects/:id', function () {

        var project = {
            name: "test 789",
            code: "789"
        };
        it('should creat a new project for the context organisation', function (done) {
            var app = express();
            app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
            app.use(projectsApi(mongodb));
            request(app)
                .put('/projects/200000000000000000000000')
                .send(project)
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.name).to.equal('test 789');
                    expect(res.body.code).to.equal('789');
                    expect(res.body.active).to.equal(true);
                    done();
                });
        })
    });

    describe('DELETE /projects/:id', function () {

        it('should creat a new project for the context organisation', function (done) {
            var app = express();
            app.use(helper.context("100000000000000000000000", "000000000000000000000000"))
            app.use(projectsApi(mongodb));
            request(app)
                .delete('/projects/200000000000000000000000')
                .expect(200)
                .end(function (err, res) {
                    if (err) done(err);
                    expect(res.body.name).to.equal('Project 1234');
                    expect(res.body.code).to.equal('1234');
                    expect(res.body.active).to.equal(false);
                    done();
                });
        })
    })

});
