var assert = require("assert");

var request = require('supertest');

var helper = require('../Helper')

var express = require('express');

var shiftsApi = require('../../src/api/ShiftsApi.js');

describe('ProjectTests', function () {

    this.timeout(10000);

    var app = express();
    var mongodb = null;

    // Connect database
    before(function (done) {
        helper.connect(function (err, db) {
            if (err) throw err;
            mongodb = db;
            done();
        });
    });

    // Start server
    before(function (done) {

        var context = function (req, res, next) {

            req.user = req.headers.user;

            req.context = {
                organisation: req.headers.organisation
            };

            if (req.params.user) req.context.user = req.params.user;
            if (req.params.organisation) req.context.organisation = req.params.organisation;

            next();
        };

        app.use("/", context, shiftsApi(mongodb));

        done();

    });

    describe('GET /shifts/:id/crew', function () {


        before(function (done) {
            helper.loadTestData("Feature_Crew", mongodb, done);
        });

        it('should return all the users with a overlapping shift', function (done) {

            request(app)
                .get('/shifts/200000000000000000000000/crew')
                .set('user', "100000000000000000000000")
                .end(function (err, res) {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.length, 4);

                    console.log(res.body)

                    assert.equal(res.body[0].user.name, 'User A');
                    assert.equal(res.body[1].user.name, 'User B');
                    assert.equal(res.body[2].user.name, 'User C');
                    assert.equal(res.body[3].user.name, 'User C');


                    assert.equal(res.body[0].shift.from, '2015-06-08T06:00:00.000Z');
                    assert.equal(res.body[0].shift.to, '2015-06-08T12:00:00.000Z');

                    done();
                });
        });
    });
});