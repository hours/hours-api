var expect = require('chai').expect

var request = require('supertest');

var helper = require('../Helper')
var express = require('express');
var index = require('../../src/index');

describe('bearer login', function () {

    this.timeout(10000);

    var app = express();
    var mongodb = null;

    var port = 9999;

    var config = require('../../config.json');

    // Connect database
    before(function (done) {
        helper.connect(function (err, db) {
            if (err) throw err;
            mongodb = db;
            done();
        });
    });

    // Start server
    before(function () {
        app.use(index(mongodb, config));
    });

    before(function (done) {
        helper.loadTestData("Auth_Login", mongodb, done);
    });

    var cookie = null;
    var token = null;

    it('login', function (done) {
        request(app)
            .post('/auth/login')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send({'username': 'test@test.nl', 'password': 'test'})
            .end(function (err, res) {
                if (err) done(err);
                cookie = res.headers['set-cookie'];
                expect(res.headers.location).to.equal('/token');
                done();
            });

    });

    it('token', function (done) {
        request(app)
            .get('/token')
            .set('Cookie', cookie)
            .end(function (err, res) {
                if (err) done(err);
                token = res.headers['x-token'];
                expect(res.headers.location).to.contains('/#access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9');
                done();
            });

    });



    it('user', function (done) {

        request(app)
            .get('/api/users/me')
            .set('Authorization', 'Bearer ' + token)
            .set('Content-Type', 'application/json')
            .end(function (err, res) {
                if (err) done(err);
                var json = JSON.parse(res.text);
                expect(json._id).to.equals('100000000000000000000000');
                done();
            });
    });

    it('user fake token', function (done) {
        var jwt = require('jwt-simple');
        request(app)
            .get('/api/users/me')
            .set('Authorization', 'Bearer ' + jwt.encode("123", "123"))
            .set('Content-Type', 'application/json')
            .end(function (err, res) {
                expect(res.status).to.equal(500);
                expect(res.text).to.contains('Error: Signature verification failed');
                done();
            });
    });

    it('user fake token', function (done) {
        var jwt = require('jwt-simple');
        request(app)
            .get('/api/users/me')
            .set('Authorization', 'Bearer ' + jwt.encode({iss:{_id:'000000000000000000000000'}}, "THIS_IS_A_LONG_SECRET_STRING"))
            .set('Content-Type', 'application/json')
            .end(function (err, res) {
                expect(res.status).to.equal(404);
                expect(res.text).to.contains('USER_NOT_FOUND');
                done();
            });
    });

});