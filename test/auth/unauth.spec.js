var expect = require('chai').expect

var request = require('supertest');

var helper = require('../Helper')
var express = require('express');
var index = require('../../src/index');

describe('unauth', function () {

    this.timeout(10000);

    var app = express();
    var mongodb = null;

    var port = 9999;

    var config = require('../../config.json');

    // Connect database
    before(function (done) {
        helper.connect(function (err, db) {
            if (err) throw err;
            mongodb = db;
            done();
        });
    });

    // Start server
    before(function () {
        app.use(index(mongodb, config));
    });

    before(function (done) {
        helper.loadTestData("Auth_Login", mongodb, done);
    });


    it('user', function (done) {

        request(app)
            .get('/api/users/me')
            .set('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
            .end(function (err, res) {
                if (err) done(err);
                expect(res.headers.location).to.equal('/');
                done();
            });
    });

    it('client', function (done) {

        request(app)
            .get('/api/users/me')
            .set('Accept', 'application/json, text/plain, */*')
            .set('Referer', 'http://localhost:8100/')
            .end(function (err, res) {
                if (err) done(err);
                expect(res.status).to.equal(401);
                done();
            });
    });

    it('subscribe', function (done) {

        request(app)
            .post('/api/subscribe')
            .set('Accept', 'application/json, text/plain, */*')
            .set('Referer', 'http://localhost:8100/')
            .send({
                email: "123123@123123.123123",
                firstname: "Willem",
                lastname: "Veelent",
                password: "123123",
                repassword: "123123"
            })
            .end(function (err, res) {
                if (err) done(err);
                expect(res.status).to.equal(200);
                done();
            });
    });

});