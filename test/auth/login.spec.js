var expect = require('chai').expect

var request = require('supertest');

var helper = require('../Helper')
var express = require('express');
var index = require('../../src/index');

describe('auth login', function () {

    this.timeout(10000);

    var app = express();
    var mongodb = null;

    var port = 9999;

    var config = require('../../config.json');

    // Connect database
    before(function (done) {
        helper.connect(function (err, db) {
            if (err) throw err;
            mongodb = db;
            done();
        });
    });

    // Start server
    before(function () {
        app.use(index(mongodb, config));
    });

    before(function (done) {
        helper.loadTestData("Auth_Login", mongodb, done);
    });

    var cookie = null;

    describe('auth login', function () {
        it('login', function (done) {
            request(app)
                .post('/auth/login')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({'username': 'test@test.nl', 'password': 'test'})
                .end(function (err, res) {
                    if (err) done(err);
                    cookie = res.headers['set-cookie'];
                    expect(res.headers.location).to.equal('/token');
                    done();
                });

        });


        it('user', function (done) {

            request(app)
                .get('/api/users/me')
                .set('Cookie', cookie)
                .set('Content-Type', 'application/json')
                .end(function (err, res) {
                    if (err) done(err);
                    var json = JSON.parse(res.text);
                    expect(json._id).to.contains('100000000000000000000000');
                    done();
                });
        });
    });

    describe('auth login cases', function () {
        it('login', function (done) {
            request(app)
                .post('/auth/login')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({'username': 'Test@Test.nl', 'password': 'test'})
                .end(function (err, res) {
                    if (err) done(err);
                    cookie = res.headers['set-cookie'];
                    expect(res.headers.location).to.equal('/token');
                    done();
                });

        });


        it('user', function (done) {

            request(app)
                .get('/api/users/me')
                .set('Cookie', cookie)
                .set('Accept', 'application/json, text/plain, */*')
                .end(function (err, res) {
                    if (err) done(err);
                    var json = JSON.parse(res.text);
                    expect(json._id).to.contains('100000000000000000000000');

                    done();
                });
        });
    });

    describe('auth login trim', function () {
        it('login', function (done) {
            request(app)
                .post('/auth/login')
                .set('Content-Type', 'application/x-www-form-urlencoded')
                .send({'username': 'test@test.nl ', 'password': 'test'})
                .end(function (err, res) {
                    if (err) done(err);
                    cookie = res.headers['set-cookie'];
                    expect(res.headers.location).to.equal('/token');
                    done();
                });

        });


        it('user', function (done) {

            request(app)
                .get('/api/users/me')
                .set('Cookie', cookie)
                .set('Accept', 'application/json, text/plain, */*')
                .end(function (err, res) {
                    if (err) done(err);
                    var json = JSON.parse(res.text);
                    expect(json._id).to.contains('100000000000000000000000');

                    done();
                });
        });
    });



});