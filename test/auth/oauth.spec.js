var expect = require('chai').expect

var request = require('supertest');

var helper = require('../Helper')
var express = require('express');
var index = require('../../src/index');

describe('ShiftLifeCycleTests', function () {

    this.timeout(10000);

    var app = express();
    var mongodb = null;

    var port = 9999;

    var config = require('../../config.json');

    // Connect database
    before(function (done) {
        helper.connect(function (err, db) {
            if (err) throw err;
            mongodb = db;
            done();
        });
    });

    // Start server
    before(function () {
        app.use(index(mongodb, config));
    });

    before(function (done) {
        helper.loadTestData("Auth_Oauth", mongodb, done);
    });


    var redirectUri = 'https://client.example.com/';
    var clientId = "300000000000000000000000";
    var clientSecret = "clientSecret";

    var cookie = null;
    var transactionId = null;
    var code = null;
    var token = null;

    it('login', function (done) {
        request(app)
            .post('/auth/login')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send({'username': 'test@test.nl', 'password': 'test'})
            .end(function (err, res) {
                if (err) done(err);
                cookie = res.headers['set-cookie'];
                expect(res.headers.location).to.equal('/token');
                done();
            });

    });

    it('token', function (done) {

        request(app)
            .get('/token')
            .set('Cookie', cookie)
            .expect(302)
            .end(function (err, res) {
                if (err) done(err);
                expect(res.headers.location).to.contains('/#access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9');
                done();
            });
    });

    it('oauth authorize get', function (done) {

        request(app)
            .get('/oauth/authorize?response_type=code&client_id=' + clientId + '&redirect_uri=' + redirectUri)
            .set('Cookie', cookie)
            .expect(200)
            .end(function (err, res) {
                if (err) done(err);
                transactionId = res.headers.transactionid;
                expect(res.text).to.contains('is requesting access to your account.');
                done();
            });
    });

    it('oauth authorize post', function (done) {

        request(app)
            .post('/oauth/authorize')
            .set('Cookie', cookie)
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send({'transaction_id': transactionId})
            .expect(302)
            .end(function (err, res) {
                if (err) done(err);
                code = res.headers.location.match(/https:\/\/client\.example\.com\/\?code=(.*)/)[1];
                expect(res.headers.location).to.contains('https://client.example.com/?code=');
                done();
            });
    });

    it('oauth token post', function (done) {

        var authorization = clientId + ":" + clientSecret;

        request(app)
            .post('/oauth/token')
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .set('Authorization', 'Basic ' + new Buffer(authorization).toString('base64'))
            .send({
                'grant_type': 'authorization_code',
                'code': code,
                'redirect_uri': redirectUri,
                'client_id': clientId
            })
            .end(function (err, res) {
                if (err) done(err);
                var json = JSON.parse(res.text);
                expect(json.access_token).to.contains('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9');
                expect(json.token_type).to.contains('Bearer');

                token = json.access_token

                done();
            });
    });

    it('user', function (done) {

        var authorization = clientId + ":" + clientSecret;

        request(app)
            .get('/api/users/me')
            .set('Content-Type', 'application/json')
            .set('Authorization', 'Bearer ' + token)

            .end(function (err, res) {
                if (err) done(err);
                done();
            });
    });

});