var init = function (req, user, data) {

    // role is owner
    if (user.role === "owner") {

        // Owner creates shift with no user
        if (data.user === null) {
            return "open";
        }

        // Owner creates shift for other user
        if (data.user !== user.id) {
            if (!data.status)
                return "assigned";
            else
                return data.status;
        }

        // Owner creates own shift
        if (!data.status)
            return "approved";
        else
            return data.status;

    }

    // other roles
    else {

        if (!data.status)
            return "pending";

        return data.status;


    }

};


module.exports = {
    init: init
};