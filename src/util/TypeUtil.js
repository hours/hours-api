var types = [
    'SHIFT',
    'EXPENSE',
    'MILEAGE',
    'BLOCKED'
]

module.exports = {

    isTypeValid: function (type) {
        return (types.indexOf(type) >= 0)
    }

};
