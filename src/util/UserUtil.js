var init = function (req, user, data) {

    var context = req.context;

    if (!data.user && data.user !== null) {
        if (context.user) {
            return context.user;
        } else {
            return req.user;
        }
    } else {
        return data.user;
    }
};


module.exports = {
    init: init
};