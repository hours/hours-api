var moment = require('moment-timezone');

var removedFilter = function (req, query) {
    query.status = {$nin: ['removed']}
};

var userFilter = function (req, user, query) {

    var context = req.context;

    if (context.user) {
        if (user.role === "worker")
            query.user = req.user;
        if (user.role === "approver" || user.role === "owner")
            query.user = context.user;
    } else {

        if (user.role === "worker")
            query.user = req.user;
        if (user.role === "approver") {
            query.user = {
                $in: user.approver.map(function (approver) {
                    return approver.id
                })
            };
            query.user.$in.push(req.user);
        }
    }
};

var projectFilter = function (req, projects, query) {
    query.project = {
        $in: projects.map(function (project) {
            return String(project._id)
        })
    };
};


var dateFilter = function (req, query) {

    var from;
    var to;

    var timezone = req.headers["x-timezone"] || "UTC";

        if (req.query.date) {
        var date = moment(req.query.date).tz(timezone).hours(0).minute(0).seconds(0).milliseconds(0)
        from = date.clone();
        to = date.clone().add(1, 'day');
    }

    if (req.query.yearweek) {
        var yearweek = req.query.yearweek;
        var year = yearweek.slice(0, 4);
        var week = yearweek.slice(4, 6)

        var date = moment(yearweek, 'YYYYWW').tz(timezone);

        from = date.clone();
        to = date.clone().add(1, 'week');

    }
    if (from && to) {
        query.$or = [
            {
                from: {
                    $gte: from.toDate(),
                    $lt: to.toDate()
                }
            }, {
                date: {
                    $gte: from.toDate(),
                    $lt: to.toDate()
                }
            }
        ];
    }

};


module.exports = {
    removedFilter: removedFilter,
    userFilter: userFilter,
    dateFilter: dateFilter,
    projectFilter: projectFilter
};
