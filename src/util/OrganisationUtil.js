var ObjectID = require('mongodb').ObjectID;

var findUserInOrganisation = function (userId, organisation) {

    var result = null;

    if (organisation.users) {
        organisation.users.forEach(function (user) {
            if (user.id === userId && user.active) {
                result = user;
            }
        })
    }
    return result;
}

module.exports = {
    findUserInOrganisation: findUserInOrganisation
};
