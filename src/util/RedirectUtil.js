module.exports = function (config) {
    return {
        url: function (req) {
            if (req.cookies) {
                var redirect = req.cookies.redirect
                if (redirect) {
                    var url = config.redirect[redirect]
                    return url
                }
            }
            return "/";
        },

        middleware: function (req, res, next) {
            var redirect = req.query.redirect
            if (redirect)
                res.cookie("redirect", redirect)
            if (req.path.match("access_token"))
                res.clearCookie("redirect")
            next();
        }
    }
};
