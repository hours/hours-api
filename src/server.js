var express = require('express');
var logger = require('express-logger');
var session = require('express-session');

var passport = require('passport');

var devicesApi = require('./api/DevicesApi');
var usersApi = require('./api/UsersApi');
var subscribeApi = require('./api/SubscribeApi');
var organisationsApi = require('./api/OrganisationsApi');
var shiftsApi = require('./api/ShiftsApi');
var tokenApi = require('./api/TokenApi');
var reportsApi = require('./api/ReportsApi');
var projectsApi = require('./api/ProjectsApi');
var categoriesApi = require('./api/CategoriesApi');
var transactionsApi = require('./api/TransactionsApi');
var documentsApi = require('./api/DocumentsApi');
var templatesApi = require('./api/TemplatesApi');
var synchronizationsApi = require('./api/SynchronizationsApi');

var localAuth = require('./auth/LocalAuth');
var facebookAuth = require('./auth/FacebookAuth');
var oAuth = require('./auth/OAuth');
var token = require('./auth/Token');

var webhookMollie = require('./mollie/WebhookMollie');

var redirectUtil = require('./util/RedirectUtil')

var cookieParser = require('cookie-parser');

var login = require('connect-ensure-login');

module.exports = function (mongodb, config) {

    var router = express.Router();

    router.get("/share/:id", function (req, res) {
        var id = req.params.id
        res.redirect("hours://share/" + id);
    });

    router.use(redirectUtil(config).middleware);

    router.use(express.static(__dirname + '/../public'));
    router.use("/lib", express.static(__dirname + '/../bower_components'));

    router.use(webhookMollie(mongodb, config));

    router.use(cookieParser());
    router.use(session({
        secret: 'my_precious',
        resave: true,
        saveUninitialized: true
    }));

    router.use(passport.initialize());
    router.use(passport.session());

    router.use("/auth", localAuth(mongodb));
    router.use("/auth", facebookAuth(mongodb, config));
    router.use("/oauth", oAuth(mongodb, config));

    router.use("/api", subscribeApi(mongodb));

    router.use(function(req, res, next){

        if(req.headers.authorization)
            return passport.authenticate('bearer', { session: false })(req, res, next);

        if (req.get('Referer') && (!req.isAuthenticated || !req.isAuthenticated()))
            return res.status(401).send();

        return next();
    });

    router.use(login.ensureAuthenticated('/'));

    var routes = [
        "/api/organisations/:organisation/users/:user",
        "/api/organisations/:organisation",
        "/api/users/:user",
        "/"
    ];
    router.use(routes, function (req, res, next) {

        if (!req.context) req.context = {};

        if (req.params.user)
            req.context.user = req.params.user;

        if (req.params.organisation)
            req.context.organisation = req.params.organisation;

        next();
    });

    router.use(token(mongodb, config));

    router.use("/api", usersApi(mongodb));
    router.use("/api", organisationsApi(mongodb));
    router.use("/api", devicesApi(mongodb));

    router.use("/api/organisations/:organisation", synchronizationsApi(mongodb));
    router.use("/api/organisations/:organisation", documentsApi(mongodb));
    router.use("/api/organisations/:organisation", projectsApi(mongodb));
    router.use("/api/organisations/:organisation", categoriesApi(mongodb));
    router.use("/api/organisations/:organisation", transactionsApi(mongodb, config));
    router.use("/api/organisations/:organisation", templatesApi(mongodb));

    router.use("/api/organisations/:organisation/users/:user", shiftsApi(mongodb));
    router.use("/api/organisations/:organisation", shiftsApi(mongodb));
    router.use("/api", shiftsApi(mongodb));

    router.use("/api/organisations/:organisation", reportsApi(mongodb));
    router.use("/api", reportsApi(mongodb));

    router.use("/api/organisations/:organisation/users/:user", tokenApi(mongodb, config));
    router.use("/api", tokenApi(mongodb, config));


    return router;
};