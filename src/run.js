var express = require('express');
var index = require('./index');

var MongoClient = require('mongodb').MongoClient;

var port = process.env.PORT || 3000;

var config = null;
    if (process.env.CONFIG) {
        config =  JSON.parse(process.env.CONFIG);
    } else {
        config =  require('../config.json');
    }


var app = express();

var mongoUrl = "mongodb://"
if (config.mongodb.username) mongoUrl += config.mongodb.username + ":";
if (config.mongodb.password) mongoUrl += config.mongodb.password + "@";
if (config.mongodb.url) mongoUrl += config.mongodb.url + ":";
if (config.mongodb.port) mongoUrl += config.mongodb.port + "/";
if (config.mongodb.db) mongoUrl += config.mongodb.db;

MongoClient.connect(mongoUrl, function (err, monogdb) {
    if (err) throw err;

    app.use(index(monogdb, config));

    app.listen(port, function () {});
});
