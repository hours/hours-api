var express = require('express');
var bodyParser = require('body-parser');

var request = require('request');

module.exports = function (mongodb, config) {

    var router = express.Router();

    router.use('/webhook', bodyParser.urlencoded({ extended: false }));

    router.post('/webhook', function (req, res, next) {

        var id = req.body.id;

        var url = "https://api.mollie.nl/v1/payments/" + id;
        var apiKey = config.mollie.apiKey;

        request.get({
            headers: {
                "Authorization": "Bearer " + apiKey
            },
            url: url
        }, function (err, response, body) {

            if(err)
                return next(err)

            if(response.statusCode !== 200)
                return res.end();

            var payment = JSON.parse(body);
            if (payment.status === "paid") {
                var query = {
                    ref: id
                }
                mongodb.collection('transactions')
                    .findOne(query, function (err, data) {
                        if (err) next(err);
                        data.status = "paid"
                        mongodb.collection('transactions')
                            .save(data, function (err) {
                                if (err) next(err);
                                res.send("OK");
                            })
                    });
            } else {
                res.end();
            }
        });
    });

    return router;
}

