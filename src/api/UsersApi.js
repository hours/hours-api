var express = require('express');
var bodyParser = require('body-parser');

var ObjectID = require('mongodb').ObjectID;

var crypto = require('crypto');

module.exports = function (mongodb) {

    var router = express.Router();

    router.use('/users', bodyParser.json());

    router.get('/users/me', function (req, res, next) {
        var id = req.user;
        var collection = mongodb.collection('users');
        collection.findOne(ObjectID(id), function (err, data) {
            if (err) return next(err);
            if (!data) return res.status(404).send("USER_NOT_FOUND");
            delete data.password;
            if (!data.name) data.name = data.firstname + " " + data.lastname;
            res.send(data);
        });
    });

    router.put('/users/me', function (req, res, next) {
        var user = req.body;

        var id = ObjectID(req.user);


        var collection = mongodb.collection('users');
        collection.findOne(ObjectID(id), function (err, data) {

            data.username = user.username;
            data.email = user.email;

            data.firstname = user.firstname;
            data.lastname = user.lastname;

            data.name = user.name || user.firstname + " " + user.lastname;

            collection.save(data, function (err) {
                if (err) return next(err);
                res.send(data);
            });
        });
    });

    return router;

};
