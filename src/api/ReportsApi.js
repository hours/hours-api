var moment = require('moment');

var fs = require('fs');
var express = require('express');
var bodyParser = require('body-parser');

var ObjectID = require('mongodb').ObjectID;

var xlsx = require('node-xlsx');
var json2csv = require('json2csv');

var async = require('async');


module.exports = function (mongodb) {

    var organisationService = require('../services/OrganisationService.js')(mongodb);
    var transactionService = require('../services/TransactionService.js')(mongodb);

    var organisationUtil = require('../util/OrganisationUtil.js');

    var router = express.Router();

    router.use('/reports', bodyParser.json());

    /**
     * Returns all the reports in the current context
     */
    router.get('/reports', function (req, res, next) {

        var context = req.context || {};

        if (!context.organisation)
            context.organisation = report.organisation;

        organisationService.findById(context.organisation, function (err, organisation) {
            if (err) return next(err);
            if (!organisation) return res.status(403).send("ORGANISATION_NOT_FOUND");

            var user = organisationUtil.findUserInOrganisation(req.user, organisation);

            if (!user) return res.status(403).send("USER_NOT_FOUND_IN_ORGANISATION");
            if (user.role != 'owner') return res.status(403).send("USER_NOT_ALLOWED_TO_VIEW_REPORT");

            var collection = mongodb.collection('reports');
            var query = {
                "user": req.user
            };

            var context = req.context;
            if (context && context.organisation) {
                query.organisation = context.organisation;
            }

            collection.find(query).toArray(function (err, reports) {
                if (err) next(err);
                res.send(reports);
            });
        });
    });

    /**
     * Generate a report
     */
    router.post('/reports', function (req, res, next) {
        var report = req.body;
        var context = req.context || {};

        if (!context.organisation)
            context.organisation = report.organisation;

        organisationService.findById(context.organisation, function (err, organisation) {

            if (err) return next(err);
            if (!organisation) return res.status(403).send("ORGANISATION_NOT_FOUND");

            var user = organisationUtil.findUserInOrganisation(req.user, organisation);
            if (!user) return res.status(403).send("USER_NOT_FOUND_IN_ORGANISATION");
            if (user.role != 'owner') return res.status(403).send("USER_NOT_ALLOWED_TO_GENERATE_REPORT");

            report.name = organisation.name;
            report.image = organisation.logo;
            report.user = req.user;

            var date = moment()
                .year(report.year)
                .month(report.month - 1)
                .date(1)
                .hours(0)
                .minutes(0)
                .seconds(0);

            report.description = date.format("MMMM") + " " + date.format("YYYY");
            report.shifts = [];

            var query = {
                "from": {
                    "$gte": date.toDate(),
                    "$lt": date.clone().add(1, 'months').toDate()
                },
                "organisation": report.organisation,
                "status": "approved"
            }

            var userIds = [];
            var projectIds = [];
            mongodb.collection('shifts').find(query).toArray(function (err, shifts) {
                if (err) return next(err);

                shifts.forEach(function (shift) {
                    userIds.push(ObjectID(shift.user));
                    projectIds.push(ObjectID(shift.project));

                    var from = moment(shift.from).add(shift.pause, "milliseconds");
                    var to = moment(shift.to);

                    shift.total = to.diff(from, "milliseconds");
                    report.shifts.push(shift);

                })

                async.parallel([

                        // load all users involved
                        function (callback) {
                            var userQuery = {
                                "_id": {$in: userIds}
                            };
                            mongodb.collection('users').find(userQuery).toArray(function (err, users) {
                                if (err) return callback(err);
                                report.users = users;
                                callback();
                            });
                        },

                        // load all projects involved
                        function (callback) {
                            var projectQuery = {
                                "_id": {$in: projectIds}
                            };
                            mongodb.collection('projects').find(projectQuery).toArray(function (err, projects) {
                                if (err) return callback(err);
                                report.projects = projects;
                                callback();
                            })
                        }
                    ],

                    //Write report to database
                    function (err, results) {
                        if (err) return next(err);
                        if (report.users.length == 0) return res.status(403).send("NO_USERS_IN_REPORT");
                        transactionService.findBalance(organisation._id.toString(), function (err, total) {

                            if(err) return next(err);
                            var amount = report.users.length * 8;
                            if (total < amount) return res.status(403).send("INSUFFICIENT_BALANCE");
                            mongodb.collection('reports')
                                .insert(report, function (err) {
                                    if (err) return next(err);
                                    var transaction = {
                                        "user": req.user,
                                        "description": "Generate report: " + report.name + " - " + report.description,
                                        "amount": (0 - amount),
                                        "date": new Date()
                                    }
                                    mongodb.collection('transactions')
                                        .insert(transaction, function (err) {
                                            if (err) return next(err);
                                            res.send(report);
                                        });
                                });
                        });
                    });
            });
        });
    });


    /**
     * Returns report by ID
     */
    router.get('/reports/:id', function (req, res, next) {
        var collection = mongodb.collection('reports');
        var id = req.params.id;
        collection.findOne(ObjectID(id), function (err, report) {
            if (err) next(err);
            res.send(report);
        });
    });

    /**
     * Returns report by ID in csv format
     */
    router.get('/reports/:id/csv', function (req, res, next) {
        var collection = mongodb.collection('reports');
        var id = req.params.id;
        collection.findOne(ObjectID(id), function (err, report) {
            if (err) next(err);
            var fields = [
                "userId",
                "userName",
                "projectId",
                "projectName",
                "from",
                "to",
                "pause",
                "total"
            ]
            var data = [];
            report.shifts.forEach(function (shift) {
                var user = findById(shift.user, report.users);
                var project = findById(shift.project, report.projects);
                data.push({
                    "userId": shift.user,
                    "userName": user.name,
                    "projectId": (shift.project) ? shift.project : "",
                    "projectName": (shift.project) ? project.name : "",
                    "from": shift.from,
                    "to": shift.to,
                    "pause": (shift.pause / 3600000),
                    "total": (shift.total / 3600000)
                });
            });
            data.sort(compareUserName)
            json2csv({data: data, fields: fields}, function (err, csv) {
                res.set('Content-Type', 'text/plain');
                res.send(new Buffer(csv).toString('base64'));
            });
        });
    });

    /**
     * Returns report by ID in csv format
     */
    router.get('/reports/:id/xlsx', function (req, res, next) {
        var collection = mongodb.collection('reports');
        var id = req.params.id;
        collection.findOne(ObjectID(id), function (err, report) {
            if (err) next(err);
            var fields = [
                "userId",
                "userName",
                "userRef",
                "projectId",
                "projectName",
                "from",
                "to",
                "pause",
                "total"
            ]
            var dataAll = [];
            var total = {};
            var pause = {};
            report.shifts.forEach(function (shift) {
                var user = findById(shift.user, report.users);
                var project = findById(shift.project, report.projects);
                dataAll.push([
                    shift.user,
                    user.name,
                    user.ref,
                    (shift.project) ? shift.project : "",
                    (shift.project) ? project.name : "",
                    shift.from,
                    shift.to,
                    (shift.pause / 3600000),
                    (shift.total / 3600000)
                ]);
                if (!total[shift.user]) total[shift.user] = 0;
                total[shift.user] += shift.total
                if (!pause[shift.user]) pause[shift.user] = 0;
                pause[shift.user] += shift.pause
            })
            dataAll.sort(compareSecond)
            dataAll.unshift(fields);

            var dataHours = [[
                "userName",
                "pauze",
                "total"
            ]]
            Object.keys(total).forEach(function (key) {
                var user = findById(key, report.users);
                dataHours.push([
                    user.name,
                    (pause[key]) ? (pause[key] / 3600000) : 0,
                    (total[key]) ? (total[key] / 3600000) : 0
                ]);
            });
            dataHours.sort(compareSecond)

            var buffer = xlsx.build([
                {
                    name: "person",
                    data: dataHours
                },
                {
                    name: "data",
                    data: dataAll
                }
            ]); // returns a buffer
            res.set('Content-Type', 'text/plain');
            res.send(buffer.toString('base64'));
        });
    });

    var findById = function (id, data) {
        var user;
        data.forEach(function (u) {
            if (String(u._id) === id) {
                user = u;
            }
        });
        return user;
    }

    var findProjectById = function (id, data) {
        var project;
        data.forEach(function (p) {
            if (String(p._id) === id) {
                project = p;
            }
        });
        return project;
    }

    function compareUserName(a, b) {
        if (a.username < b.username)
            return -1;
        if (a.username > b.username)
            return 1;
        return 0;
    }

    function compareSecond(a, b) {
        if (a[1] < b[1])
            return -1;
        if (a[1] > b[1])
            return 1;
        return 0;
    }


    return router;

};
