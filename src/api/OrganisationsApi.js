var express = require('express');
var bodyParser = require('body-parser');

var defaultFeatures = {app: ['reports']};

module.exports = function (mongodb) {

    var router = express.Router();

    router.use('/organisations', bodyParser.json());

    var userService = require('../services/UserService.js')(mongodb);
    var organisationService = require('../services/OrganisationService.js')(mongodb);
    var organisationUtil = require('../util/OrganisationUtil.js');

    function fixOrganisationSettings(organisation){

        organisation.settings = {
            multipleshifts: organisation.multipleshifts,
            pause: organisation.pause,
            swap: organisation.swap,
            availability: organisation.availability
        };
        organisation.multipleshifts = undefined;
        organisation.pause = undefined;
        organisation.swap = undefined;
        organisation.availability = undefined;
        return organisation;
    }

    /*
     * Return all the organisations where the conext user has a role in
     */
    router.get('/organisations', function (req, res, next) {

        var userId = req.user
        var role = req.query.role;

        var organisations = [];
        organisationService.findByUserId(userId, function (err, data) {
            if (err) next(err);
            data.forEach(function (organisation) {
                var user = organisationUtil.findUserInOrganisation(userId, organisation);
                if (!role || user.role === role) {
                    organisation.role = user.role
                    organisation.users = undefined;
                    organisations.push(organisation);
                }

                // Fix for data structure migration
                if(!organisation.settings)
                    fixOrganisationSettings(organisation)

                if (!organisation.features)
                    organisation.features = defaultFeatures;

            });
            res.send(organisations);
        });
    });

    router.get('/organisations/:id', function (req, res, next) {

        var id = req.params.id;
        var userId = req.user

        organisationService.findById(id, function (err, organisation) {
            if (err) next(err);
            if (!organisation)
                return res.status(403).send('ORGANISATION_NOT_FOUND');

            var user = organisationUtil.findUserInOrganisation(userId, organisation);
            if (!user)
                return res.status(403).send('USER_NOT_FOUND_IN_ORGANISATION');

            organisation.role = user.role;
            organisation.users = undefined;

            // Fix for data structure migration
            if(!organisation.settings)
                fixOrganisationSettings(organisation)

            if (!organisation.features)
                organisation.features = defaultFeatures;

            return res.send(organisation);

        });
    });

    router.post('/organisations', function (req, res, next) {

        var userId = req.user
        var organisation = req.body;

        var user = {
            "id": userId,
            "role": "owner",
            "active": true
        };

        organisation.users = [];
        organisation.users.push(user);

        organisationService.save(organisation, function (err, organisation) {
            if (err) next(err);
            return res.send(organisation);
        });
    });

    router.put('/organisations/:id', function (req, res, next) {

        var id = req.params.id;
        var userId = req.user;
        var organisation = req.body;

        organisationService.findById(id, function (err, data) {
            if (err) return next(err);

            var user = organisationUtil.findUserInOrganisation(userId, data);
            if (!user) return res.status(403).send("USER_NOT_FOUND_IN_ORGANISATION");
            if (user.role !== "owner") return res.status(403).send("NOT_ALLOWED_TO_UPDATE_ORGANISATION_IN_THIS_ROLE");

            organisation._id = data._id;
            organisation.users = data.users;
            organisation.features = data.features;

            organisationService.save(organisation, function (err) {
                if (err) return next(err);
                return res.send(organisation);
            });
        })
    });

    /*
     * Remove organisation
     */
    router.delete('/organisations/:id', function (req, res, next) {

        var id = req.params.id;
        var userId = req.user;

        organisationService.findById(id, function (err, organisation) {
            if (err) return next(err);
            organisationService.deleteUser(organisation, userId, function (err) {
                if (err) return next(err);
                return res.send(organisation);
            });
        })
    });

    router.post('/organisations/:id/join', function (req, res, next) {

        var id = req.params.id;
        var userId = req.user;

        organisationService.findById(id, function (err, organisation) {

            if (err) return next(err);
            if (!organisation) return res.status(403).send("ORGANISATION_NOT_FOUND");
            organisationService.addUser(organisation, userId, function (err, data) {
                if (err) return next(err);
                return res.send(data);
            });
        })
    });

    router.get('/organisations/:id/users', function (req, res, next) {
        var id = req.params.id;
        var userId = req.user

        organisationService.findById(id, function (err, organisation) {
            if (err) return next(err);
            if (!organisation) return res.status(403).send("ORGANISATION_NOT_FOUND");

            if (!organisation.users)  return res.send([]);

            var user = organisationUtil.findUserInOrganisation(userId, organisation);
            if (!user) return res.status(403).send("USER_NOT_FOUND_IN_ORGANISATION");

            var users = []
            if (user.role === "owner") users = organisation.users
            if (user.role === "approver") {
                users = []
                user.approver.forEach(function (x) {
                    users.push({
                        id: x.id,
                        active: x.active,
                        ref: x.ref,
                        contract: x.contract
                    })
                })
            }
            if (user.role === "approver" || user.role === "worker") {
                users.push(user)
            }
            userService.findByUsers(users, function (err, data) {
                if (err) return next(err);
                return res.send(data);
            })
        });
    });

    router.post('/organisations/:id/users', function (req, res, next) {

        var id = req.params.id;
        var userId = req.body._id
        organisationService.findById(id, function (err, organisation) {

            if (err) return next(err);
            if (!organisation) return res.status(403).send("ORGANISATION_NOT_FOUND");
            var requester = organisationUtil.findUserInOrganisation(req.user, organisation);
            if (requester.role === 'owner'){
                organisationService.addUser(organisation, userId, function (err, data) {
                    if (err) return next(err);
                    return res.send(data);
                });
            }else return res.status(403).send("NOT_ALLOWED_TO_ADD_EMPLOYEES_IN_THIS_ROLE");
        })
    });


    router.get('/organisations/:organisationId/users/:userId', function (req, res, next) {
        var userId = req.params.userId;
        var organisationId = req.params.organisationId;

        organisationService.findById(organisationId, function (err, organisation) {
            if (err) return next(err);
            if (!organisation.users)  return res.send([]);
            var user = organisationUtil.findUserInOrganisation(userId, organisation);

            if (user) {
                userService.findById(userId, function (err, data) {
                    if (err) return next(err);
                    return res.send(data);
                });
            } else {
                res.end();
            }
        });
    });

    /*
     * Update user in organisation
     */
    router.put('/organisations/:organisationId/users/:userId', function (req, res, next) {

        var organisationId = req.params.organisationId;
        var userId = req.params.userId;

        organisationService.findById(organisationId, function (err, organisation) {
            if (err) return next(err);

            var data = req.body;
            var user = organisationUtil.findUserInOrganisation(userId, organisation);

            if (!user) return res.status(403).send("Not allowed to update organisation");

            if (user.id === userId && user.active) {
                user.ref = data.ref;
                user.approver = data.approver;
                user.contract = data.contract;
                if (user.approver && user.role != "owner") {
                    if (user.approver.length > 0) {
                        user.role = "approver";
                    } else {
                        user.role = "worker";
                    }
                }
            }

            organisationService.save(organisation, function (err) {
                if (err) return next(err);
                return res.send(organisation);
            });

        });

    });

    /*
     * Remove user from organisation
     */
    router.delete('/organisations/:organisationId/users/:userId', function (req, res, next) {

        var organisationId = req.params.organisationId;
        var userId = req.params.userId;

        organisationService.findById(organisationId, function (err, organisation) {
            if (err) return next(err);

            var data = req.body;
            var user = organisationUtil.findUserInOrganisation(userId, organisation);

            if (!user) return res.status(403).send("NOT_ALLOWED_TO_UPDATE_ORGANISATION");

            user.active = false;

            organisationService.save(organisation, function (err) {
                if (err) return next(err);
                return res.send(organisation);
            });

        })
    });

    return router;

};
