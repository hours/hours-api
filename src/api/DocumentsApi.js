var express = require('express');
var bodyParser = require('body-parser');

module.exports = function (mongodb) {

    var router = express.Router();
    router.use('/documents', bodyParser.json());

    var types = [
        'CASH_STATE',
        'CHECKLIST',
        'COMPLAINT',
        'PROCEDURE',
        'PIN_ONLY'
    ];

    /*
     * Return all the documents from the organisatoin
     */
    router.get('/documents', function (req, res, next) {
        var query = {
            organisation: req.context.organisation,
        };

        if(req.query.type)
            query.type = req.query.type

        mongodb.collection('documents').find(query).toArray(function (err, documents) {
            if (err) return next(err);
            res.send(documents);
        });
    });

    /*
     * Creates a new document for the organisatoin
     */
    router.post('/documents', function (req, res, next) {
        var document = req.body;

        // Check if type exists
        if(types.indexOf(document.type) < 0){
            return res.status(403).send("TYPE_OF_DOCUMENT_DOES_NOT_EXIST");
        }

        document.organisation = req.context.organisation;

        mongodb.collection('documents').insert(document, function (err) {
            if (err) callback(err);
            res.send(document);
        });

    });

    return router;

};


