var express = require('express');
var bodyParser = require('body-parser');

var moment = require('moment');
var request = require('request');


module.exports = function (mongodb, config) {

    var transactionService = require('../services/TransactionService.js')(mongodb);


    var router = express.Router();

    router.use('/transactions', bodyParser.json());

    router.get('/transactions', function (req, res, next) {
        var query = {
            organisation: req.context.organisation,
            $or: [
                {status: "paid"},
                {status: {$exists: false}}
            ]
        };
        mongodb.collection('transactions')
            .find(query).toArray(function (err, transactions) {
            if (err) return next(err);
            res.send(transactions);
        });
    });

    router.get('/transactions/balance', function (req, res, next) {
        transactionService.findBalance(req.context.organisation, function (err, total) {
            if (err) return next(err);
            res.send(total.toString());
        });
    });

    router.post('/transactions', function (req, res, next) {

        var transaction = req.body;

        var url = "https://api.mollie.nl/v1/payments";
        var apiKey = config.mollie.apiKey;

        request.post({
            headers: {
                "Authorization": "Bearer " + apiKey
            },
            url: url,
            form: {
                amount: transaction.amount,
                description: transaction.credits + " credits",
                redirectUrl: "http://hours-app.com/"
            }
        }, function (err, response, body) {
            if(err) return res.status(403).send("CANNOT_CREATE_PAYMENT");
            var payment = JSON.parse(body);
            var data = {
                user: req.user,
                organisation: req.context.organisation,
                description: "Add credits",
                amount: transaction.credits,
                date: new Date(),
                ref: payment.id,
                status: "open"
            }

            mongodb.collection('transactions')
                .insert(data, function (err) {
                    if (err) next(err);
                    res.send(body);
                });
        });

    });

    return router;

};
