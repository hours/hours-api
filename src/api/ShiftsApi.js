var express = require('express');
var async = require('async');
var bodyParser = require('body-parser');

var moment = require('moment');
var ObjectID = require('mongodb').ObjectID;

var defaultFilters = [
    'userFilter',
    'dateFilter',
    'removedFilter'
];

module.exports = function (mongodb) {

    var projectService = require('../services/ProjectService.js')(mongodb);
    var shiftService = require('../services/ShiftService.js')(mongodb);
    var organisationService = require('../services/OrganisationService.js')(mongodb);
    var syncService = require('../services/SyncService.js')(mongodb);

    var typeUtil = require('../util/TypeUtil.js');

    var filterUtil = require('../util/FilterUtil.js');
    var organisationUtil = require('../util/OrganisationUtil.js');

    var router = express.Router();

    router.use('/shifts', bodyParser.json({limit: '1mb'}));

    // Find organisation
    router.use('/shifts', function (req, res, next) {

        var shift = req.body;

        var organisationId = req.context.organisation || shift.organisation;

        if (!organisationId)
            return next();

        organisationService.findById(organisationId, function (err, organisation) {
            if (err) return next(err);
            req.organistion = organisation;
            next();
        });

    });

    // Find workflow
    router.use('/shifts', function (req, res, next) {

        var workflow = null;

        if (req.organistion && req.organistion.workflow)
            workflow = req.organistion.workflow;
        else
            workflow = 'DefaultWorkflow';

        req.workfow = require('../workflow/' + workflow + '.js')(mongodb);
        next();
    });

    router.get('/shifts', function (req, res, next) {

        var context = req.context;

        if (context.organisation) {
            organisationService.findById(context.organisation, function (err, organisation) {
                    if (err) next(err);

                    var user = organisationUtil.findUserInOrganisation(req.user, organisation);

                    var collection = mongodb.collection('shifts');
                    var query = {
                        "organisation": context.organisation
                    };

                    var exec = [];

                    if (!organisation.filters)
                        organisation.filters = defaultFilters;


                    if (organisation.filters.indexOf('userFilter') >= 0)
                        exec.push(function (callback) {
                            filterUtil.userFilter(req, user, query);
                            callback()
                        });

                    if (organisation.filters.indexOf('dateFilter') >= 0)
                        exec.push(function (callback) {
                            filterUtil.dateFilter(req, query);
                            callback()
                        });

                    if (organisation.filters.indexOf('removedFilter') >= 0)
                        exec.push(function (callback) {
                            filterUtil.removedFilter(req, query);
                            callback()
                        });


                    if (organisation.filters.indexOf('projectFilter') >= 0)
                        exec.push(function (callback) {
                            projectService.findByUserId(req.user, context.organisation, function (err, projects) {
                                filterUtil.projectFilter(req, projects, query);
                                callback();
                            });
                        });


                    if (req.query.status)
                        query.status = req.query.status;

                    if (req.query.type)
                        query.type = req.query.type;

                    async.series(exec, function () {

                        collection.find(query).toArray(function (err, shifts) {
                            if (err) next(err);

                            shifts.map(function (x) {
                                if (!x.date)
                                    x.date = moment(x.from).startOf('day').toDate();

                                // Fix for migration
                                if (!x.type)
                                    x.type = 'SHIFT';

                                return x

                            });

                            res.send(shifts);

                        });
                    });
                }
            )
        } else {
            res.end();
        }
    });

    router.get('/shifts/count', function (req, res, next) {
        var context = req.context

        if (context.organisation) {
            organisationService.findById(context.organisation, function (err, organisation) {
                if (err) next(err);

                var user = organisationUtil.findUserInOrganisation(req.user, organisation);

                if(!user)
                    return res.status(403).send("USER_HAS_NO_ROLE_IN_ORGANISATION");

                var collection = mongodb.collection('shifts');

                var query = {
                    "organisation": req.context.organisation
                };

                filterUtil.userFilter(req, user, query)
                filterUtil.dateFilter(req, query);

                if (req.query.status)
                    query.status = req.query.status;

                if (req.query.type)
                    query.type = req.query.type;

                collection.find(query).count(function (err, count) {
                    if (err) next(err);
                    res.send(String(count));
                });

            });
        } else {
            return res.status(403).send("Organisation is required");
        }
    });

    router.post('/shifts', function (req, res, next) {

        var shift = req.body;
        var context = req.context;

        // Set default type
        if (!shift.type)
            shift.type = 'SHIFT';

        // Validate type
        if (!typeUtil.isTypeValid(shift.type))
            return res.status(403).send("TYPE_DOES_NOT_EXIST");

        if (!shift.organisation && !context.organisation)
            return res.status(403).send("NO_ORGANISATION_FOUND");

        if (!shift.user && !context.user && !req.user)
            return res.status(403).send("NO_USER_FOUND");

        organisationService.findById(shift.organisation || context.organisation, function (err, organisation) {
            if (err) return next(err);

            var workflow = req.workfow;
            workflow.create(req, shift, organisation, function (err, ret) {
                if (err)return res.status(403).send(err);
                shiftService.save(ret, function (err, data) {
                    if (err)return res.status(403).send(err);
                    syncService.sync(data.organisation);
                    return res.send(data);
                });
            });

        });
    });

    router.get('/shifts/:id', function (req, res, next) {
        var id = req.params.id;
        var collection = mongodb.collection('shifts');
        collection.findOne(ObjectID(id), function (err, data) {
            if (err) next(err);
            res.send(data);
        });
    });

    router.put('/shifts/:id', function (req, res, next) {

        var id = req.params.id;
        var shift = req.body;

        shiftService.findById(id, function (err, data) {
            if (err) return next(err);

            organisationService.findById(data.organisation, function (err, organisation) {
                if (err) return next(err);

                var workflow = req.workfow
                workflow.update(req, shift, organisation, data, function (err, ret) {
                    shiftService.save(ret, function (err, data) {
                        if (err) return res.status(403).send(err);
                        syncService.sync(data.organisation);
                        res.send(data);
                    });
                });
            });
        });
    });

    router.delete('/shifts/:id', function (req, res, next) {
        var id = req.params.id;

        shiftService.findById(id, function (err, shift) {

            if (err)
                return next(err);

            var workflow = req.workfow
            workflow.remove(req, shift, function (err, data) {
                if (err)
                    return res.status(403).send(err);

                syncService.sync(data.organisation);
                return res.send(data);

            });
        });
    });

    router.post('/shifts/:id/:action', function (req, res, next) {

        var id = req.params.id;
        var action = req.params.action;

        var workflow = req.workfow;

        if (!workflow[action])
            return res.status(403).send('ACTION_NOT_DEFINED_IN_WORKFLOW');

        shiftService.findById(id, function (err, shift) {

            if (err)
                return next(err);

            workflow[action](req, shift, function (err, shift) {

                if (err)
                    return res.status(403).send(err);

                shiftService.save(shift, function (err, shift) {
                    if (err) return next(err);
                    return res.send(shift);
                });

            });
        });
    });

    router.get('/shifts/:id/crew', function (req, res, next) {
            var id = req.params.id;
            mongodb.collection('shifts').find(ObjectID(id)).limit(1).next(function (err, shift) {
                if (err) next(err);


                var check=moment(shift.from).format('DD');
                var start = (check!=shift.from.getDate()) ? shift.from : shift.from.setHours(0,0,0,0);
                var end = shift.from.setHours(23,59,59,59);

                var query = {
                    organisation: shift.organisation,
                    from: {$gte: new Date(start), $lt: new Date(end)}
                };

                mongodb.collection('shifts').find(query).toArray(function (err, shifts) {
                    if (err) next(err);

                    var userIds = shifts.map(function (shift) {
                        return ObjectID(shift.user)
                    });

                    var query = {
                        '_id': {'$in': userIds}
                    };


                    mongodb.collection('users').find(query).sort({name: 1}).toArray(function (err, users) {
                        if (err) next(err);

                        var usersObj = {};
                        var result = [];

                        users.forEach(function (user) {
                            if (!user.name)
                                user.name = user.firstname + ' ' + user.lastname
                            usersObj[user._id] = user;
                        });


                        shifts.forEach(function (shift) {
                            result.push({
                                shift: shift,
                                user: usersObj[shift.user]
                            })
                        });

                        res.send(result);

                    });


                });
            });
        });

        return router;

    };
