var express = require('express');
var bodyParser = require('body-parser');

var ObjectID = require('mongodb').ObjectID;

module.exports = function (mongodb) {

    var projectService = require('../services/ProjectService.js')(mongodb);
    var organisationService = require('../services/OrganisationService.js')(mongodb);

    var organisationUtil = require('../util/OrganisationUtil.js');

    var router = express.Router();

    router.use('/projects', bodyParser.json());

    /*
     * Return all the projects from the organisatoin
     */
    router.get('/projects', function (req, res, next) {

        var context = req.context;

        if (context.organisation) {


            organisationService.findById(context.organisation, function (err, organisation) {
                if (err) next(err);

                if (!organisation)
                    return res.status(403).send('ORGANISATION_NOT_FOUND');

                var user = organisationUtil.findUserInOrganisation(req.user, organisation);
                if (!user)
                    return res.status(403).send('USER_NOT_FOUND_IN_ORGANISATION');

                if (user.role === 'owner') {
                    projectService.findByOrganisationId(context.organisation, function (err, projects) {
                        if (err) return next(err);
                        res.send(projects);
                    });
                } else {
                    projectService.findByUserId(req.user, context.organisation, function (err, projects) {
                        if (err) return next(err);
                        res.send(projects);
                    });
                }

            })

        }

    });

    /*
     * Creates a new project for the organisatoin
     */
    router.post('/projects', function (req, res, next) {
        var project = req.body;
        project.organisation = req.context.organisation;
        project.active = true;
        mongodb.collection('projects').insert(project, function (err) {
            if (err) callback(err);
            res.send(project);
        });
    });

    /*
     * Return one the projects from the organisation
     */
    router.get('/projects/:id', function (req, res, next) {
        var id = req.params.id;
        mongodb.collection('projects').findOne(ObjectID(id), function (err, data) {
            if (err) return next(err);
            res.send(data);
        });
    });

    /*
     * Creates a new project for the organisatoin
     */
    router.put('/projects/:id', function (req, res, next) {
        var id = req.params.id;
        var project = req.body;

        mongodb.collection('projects').findOne(ObjectID(id), function (err, data) {
            if (err) return next(err);
            data.code = project.code;
            data.name = project.name;
            data.users = project.users;
            mongodb.collection('projects').save(data, function (err) {
                if (err) callback(err);
                res.send(data);
            });
        });

    });

    /*
     * Creates a new project for the organisatoin
     */
    router.delete('/projects/:id', function (req, res, next) {
        var id = req.params.id;

        mongodb.collection('projects').findOne(ObjectID(id), function (err, data) {
            if (err) return next(err);
            data.active = false;
            mongodb.collection('projects').save(data, function (err) {
                if (err) callback(err);
                res.send(data);
            });
        });

    });

    return router;

};
