var express = require('express');
var bodyParser = require('body-parser');

module.exports = function (mongodb) {

    var router = express.Router();

    router.use('/devices', bodyParser.json());

    /*
     * Return all the devices from the organisatoin
     */
    router.get('/devices', function (req, res, next) {
        var query = {
            user: req.user
        };

        mongodb.collection('devices').find(query).toArray(function (err, devices) {
            if (err) return next(err);
            res.send(devices);
        });
    });

    /*
     * Creates a new device for the organisatoin
     */
    router.post('/devices', function (req, res, next) {

        var device = req.body;

        if (!device.uuid)
            return res.status(403).send('DEVICE_UUID_NOT_SET');

        var collection = mongodb.collection('devices');
        collection.ensureIndex("uuid", {unique: true}, function (err) {
            if (err) return next(err);

            var query = {uuid: device.uuid}
            collection.findOne(query, function (err, data) {
                if (err) return next(err);
                if (!data) data = {};
                data.user = req.user;
                data.uuid = device.uuid;
                data.model = device.model;
                data.platform = device.platform;
                data.version = device.version;
                data.manufacturer = device.manufacturer;
                data.serial = device.serial;
                data.registration = device.registration;
                data.updated = new Date();

                collection.save(data, function (err) {
                    if (err) return next(err);
                    res.send(data);
                });

            });
        });
    });

    return router;

};


