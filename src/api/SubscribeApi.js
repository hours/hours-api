var express = require('express');
var bodyParser = require('body-parser');

var ObjectID = require('mongodb').ObjectID;

var crypto = require('crypto');

module.exports = function (mongodb) {

    var router = express.Router();

    router.use('/subscribe', bodyParser.json());

    /*
     * Subscribe as a new user
     */
    router.post('/subscribe', function (req, res, next) {

        var query = {
            username: req.body.email
        };
        mongodb.collection('users').find(query).count(function (err, count) {
            if (err) return next(err);
            if (count > 0) return res.status(403).send("USER_ALREADY_EXISTS");
            var hash = crypto.createHash('sha1').update(req.body.password).digest('hex');
            var user = {
                username: req.body.email,
                password: hash,
                email: req.body.email,
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                name: req.body.name || req.body.firstname + " " + req.body.lastname,
                provider: "local"
            };
            mongodb.collection('users').insert(user, function (err) {
                if (err) return next(err);
                res.send(user);
            });
        });
    });

    return router;

};
