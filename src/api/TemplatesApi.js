var express = require('express');
var bodyParser = require('body-parser');

var ObjectID = require('mongodb').ObjectID;

module.exports = function (mongodb) {

    var templateService = require('../services/TemplateService.js')(mongodb);

    var router = express.Router();

    router.use('/templates', bodyParser.json());

    router.get('/templates', function (req, res, next) {

        var organisation = null;
        if (req.context && req.context.organisation) {
            organisation = req.context.organisation;
        }

        templateService.findByOrganisation(organisation, function (err, data) {
            if (err) return next(err);
            res.send(data)
        })


    });

    router.post('/templates', function (req, res, next) {

        var template = req.body;

        var collection = mongodb.collection('templates');
        var data = {}
        data.organisation = template.organisation;
        data.type = template.type;
        data.shifts = template.shifts;
        collection.insertOne(data, function (err) {
            if (err) return callback(err);
            res.send(data);
        });

    });

    router.put('/templates/:id', function (req, res, next) {

        var id = req.params.id;
        var template = req.body;

        var collection = mongodb.collection('templates');

        collection.findOne(ObjectID(id), function (err, data) {
            if (err) return next(err);
            data.organisation = template.organisation;
            data.type = template.type;
            data.shifts = template.shifts;
            collection.save(data, function (err) {
                if (err) return callback(err);
                res.send(data);
            });
        });
    });

    return router;

};
