var express = require('express');
var bodyParser = require('body-parser');

var ObjectID = require('mongodb').ObjectID;

module.exports = function (mongodb) {

    var router = express.Router();

    router.use('/categories', bodyParser.json());

    /*
     * Return all the categories from the organisatoin
     */
    router.get('/categories', function (req, res, next) {
        var query = {
            organisation: req.context.organisation,
            active: true
        };
        mongodb.collection('categories').find(query).toArray(function (err, categories) {
            if (err) return next(err);
            res.send(categories);
        });
    });

    /*
     * Creates a new category for the organisatoin
     */
    router.post('/categories', function (req, res, next) {
        var category = req.body;
        category.organisation = req.context.organisation;
        category.active = true;
        mongodb.collection('categories').insert(category, function (err) {
            if (err) callback(err);
            res.send(category);
        });
    });

    /*
     * Creates a new category for the organisatoin
     */
    router.put('/categories/:id', function (req, res, next) {
        var id = req.params.id;
        var category = req.body;

        mongodb.collection('categories').findOne(ObjectID(id), function (err, data) {
            if (err) return next(err);
            data.code = category.code;
            data.name = category.name;
            mongodb.collection('categories').save(data, function (err) {
                if (err) callback(err);
                res.send(data);
            });
        });

    });

    /*
     * Creates a new category for the organisatoin
     */
    router.delete('/categories/:id', function (req, res, next) {
        var id = req.params.id;

        mongodb.collection('categories').findOne(ObjectID(id), function (err, data) {
            if (err) return next(err);
            data.active = false;
            mongodb.collection('categories').save(data, function (err) {
                if (err) callback(err);
                res.send(data);
            });
        });

    });

    return router;

};


