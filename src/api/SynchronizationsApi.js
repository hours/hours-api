var express = require('express');
var bodyParser = require('body-parser');

var ObjectID = require('mongodb').ObjectID;

module.exports = function (mongodb) {

    var router = express.Router();
    router.use('/synchronizations', bodyParser.json());

    /*
     * Return all the documents from the organisatoin
     */
    router.get('/synchronizations', function (req, res, next) {
        var query = {
            organisation: req.context.organisation
        };

        mongodb.collection('synchronizations').find(query).toArray(function (err, synchronizations) {
            if (err) return next(err);
            res.send(synchronizations);
        });
    });

    /*
     * Creates a new synchronization for the organisation
     */
    router.post('/synchronizations', function (req, res, next) {

        if (!req.context.organisation)
            return res.status(403).send("NO_ORGANISATION_FOUND");

        var synchronization = req.body;

        var data = {};
        data.name = synchronization.name;
        data.type = synchronization.type;
        data.organisation = req.context.organisation;
        data.params = synchronization.params;
        data.active = synchronization.active;

        mongodb.collection('synchronizations').insert(data, function (err) {
            if (err) next(err);
            res.send(synchronization);
        });

    });

    /*
     * Update a synchronization for the organisation
     */
    router.put('/synchronizations/:id', function (req, res, next) {

        if (!req.context.organisation)
            return res.status(403).send("NO_ORGANISATION_FOUND");

        var id = req.params.id;
        var synchronization = req.body;

        mongodb.collection('synchronizations').findOne(ObjectID(id), function (err, data) {

            if (err)
                return next(err);

            data.name = synchronization.name;
            data.type = synchronization.type;
            data.organisation = req.context.organisation;
            data.params = synchronization.params;
            data.active = synchronization.active;

            mongodb.collection('synchronizations').save(data, function (err) {
                if (err) next(err);
                res.send(data);
            });

        });


    });

    return router;

};


