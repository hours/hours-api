var passport = require('passport');
var jwt = require('jwt-simple');
var BearerStrategy = require('passport-http-bearer').Strategy;

var server = require('./server');

module.exports = function (monogdb, config) {

    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user._id);
    });

    passport.use(new BearerStrategy({},
        function (token, done) {
            process.nextTick(function () {
                try {
                    var user = jwt.decode(token, config.jwtTokenSecret);
                    return done(null, user.iss._id, {scope: 'all'});
                } catch (err) {
                    done(err)
                }
            });
        }
    ));


    return server(monogdb, config);
};
