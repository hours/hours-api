var ObjectID = require('mongodb').ObjectID;

var projectService = function (mongodb) {


    return {

        findById: function (id, callback) {
            try {
                var query = {_id: ObjectID(id)}
                mongodb.collection('projects').findOne(query, function (err, data) {
                    if (err) callback(err);
                    callback(null, data);
                })
            } catch (e) {
                callback(e);
            }

        },

        findByOrganisationId: function (organisationId, callback) {

            var query = {
                organisation: organisationId
            };

            mongodb.collection('projects').find(query).toArray(function (err, data) {
                if (err) callback(err);
                callback(null, data);
            });

        },

        findByUserId: function (userId, organisationId, callback) {

            var query = {
                $or: [
                    {
                        organisation: organisationId,
                        users: null,
                        active: true
                    },
                    {
                        organisation: organisationId,
                        users: {$exists: false},
                        active: true
                    },
                    {
                        organisation: organisationId,
                        users: {
                            $elemMatch: {
                                id: userId
                            }
                        },
                        active: true
                    }
                ]
            };

            mongodb.collection('projects').find(query).toArray(function (err, data) {
                if (err) callback(err);
                callback(null, data);
            });

        }
    }
};

module.exports = projectService;
