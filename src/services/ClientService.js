var ObjectID = require('mongodb').ObjectID;

var service = function (mongodb) {

    return {

        findById: function (id, callback) {
            var query = {_id: ObjectID(id)};
            mongodb.collection('clients').findOne(query, function (err, data) {
                if (err) return callback(err);
                callback(null, data);
            })
        }

    }
};

module.exports = service;
