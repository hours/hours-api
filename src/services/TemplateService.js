var ObjectID = require('mongodb').ObjectID;

var service = function (mongodb) {

    return {

        findById: function (id, callback) {
            var query = {_id: ObjectID(id)}
            mongodb.collection('templates').findOne(query, function (err, data) {
                if (err) callback(err);
                callback(null, data);
            })
        },

        findByOrganisation: function (organisation, callback) {
            var query = {organisation: organisation}
            mongodb.collection('templates').find(query).toArray(function (err, data) {
                if (err) callback(err);
                callback(null, data);
            })
        }
    }
};

module.exports = service;
