

var service = function (mongodb) {

    var hoursSync = require('hours-sync')(mongodb);

    return {

        sync: function (id, callback) {

            try{
                hoursSync.sync(id, callback)
            }
            catch(e){
                console.error('Error in sync:', e)
            }

        }

    }
};

module.exports = service;
