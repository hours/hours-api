var ObjectID = require('mongodb').ObjectID;

var service = function (mongodb) {

    return {

        findById: function (id, callback) {
            var query = {_id: ObjectID(id)}
            mongodb.collection('users').findOne(query, function (err, data) {
                if (err) callback(err);
                if (!data.name) data.name = data.firstname + " " + data.lastname;
                callback(null, data);
            })
        },

        findByRef: function (ref, provider, callback) {
            var query = {
                ref: ref,
                provider: provider
            };
            mongodb.collection('users').findOne(query, function (err, data) {
                if (err) callback(err);
                if (!data.name) data.name = data.firstname + " " + data.lastname;
                callback(null, data);
            });
        },

        findByUsers: function (users, callback) {

            var ids = [];
            var map = {};
            users.forEach(function (user) {
                if (user.active) {
                    ids.push(ObjectID(user.id));
                    map[user.id] = user
                }
            });

            var query = {
                "_id": {$in: ids}
            };

            mongodb.collection('users').find(query).toArray(function (err, users) {
                if (err) callback(err);
                users.forEach(function (user) {
                    if (!user.name) user.name = user.firstname + " " + user.lastname;
                    user.ref = map[user._id].ref;
                    user.role = map[user._id].role;
                    user.approver = map[user._id].approver;
                    user.contract = map[user._id].contract;
                });
                callback(null, users);
            });
        }
    }
}

module.exports = service;
