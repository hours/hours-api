var ObjectID = require('mongodb').ObjectID;

var organisationUtil = require('../util/OrganisationUtil.js');

var organisationService = function (mongodb) {


    return {

        findById: function (id, callback) {
            try{
                var query = {_id: ObjectID(id)}
                mongodb.collection('organisations').findOne(query, function (err, data) {
                    if (err) callback(err);
                    callback(null, data);
                })
            }catch (e){
                callback(e);
            }

        },

        findByUserId: function (userId, callback) {

            var query = {
                users: {
                    $elemMatch: {
                        id: userId,
                        active: true
                    }
                }
            };

            var organisations = [];
            mongodb.collection('organisations').find(query).toArray(function (err, data) {
                if (err) callback(err);
                data.forEach(function (organisation) {
                    organisations.push(organisation)
                });
                callback(null, organisations);
            });

        },

        save: function (organisation, callback) {
            var data = {
                _id: organisation._id,
                name: organisation.name,
                logo: organisation.logo,
                description: organisation.description,

                users: organisation.users,
                settings: organisation.settings,
                features: organisation.features
            };

            if (organisation.settings) {
                data.multipleshifts = organisation.settings.multipleshifts;
                data.pause = organisation.settings.pause;
                data.availability = organisation.settings.availability;
                data.swap = organisation.settings.swap;
            }

            mongodb.collection('organisations').save(data, function (err) {
                if (err) callback(err);
                callback(null, data);
            })
        },

        addUser: function (organisation, userId, callback) {
            var add = true;
            organisation.users.forEach(function (user, index) {
                if (user.id === userId) {
                    add = false
                    user.active = true;
                }
                ;
            })
            if (add) {
                var user = {
                    "id": userId,
                    "role": "worker",
                    "active": true
                }
                organisation.users.push(user);
            }
            mongodb.collection('organisations').save(organisation, function (err) {
                if (err) callback(err);
                callback(null, organisation);
            })
        },

        saveUser: function (organisation, user, callback) {
            if (organisationUtil.findUserInOrganisation(userId, organisation)) {
                organisation.users.forEach(function (user, index) {
                    if (user.id === userId) {
                        user.active = false;
                    }
                    ;
                })
            }
            mongodb.collection('organisations').save(organisation, function (err) {
                if (err) callback(err);
                callback(null, organisation);
            })
        },

        deleteUser: function (organisation, userId, callback) {
            if (organisationUtil.findUserInOrganisation(userId, organisation)) {
                organisation.users.forEach(function (user, index) {
                    if (user.id === userId) {
                        user.active = false;
                    }
                    ;
                })
            }
            mongodb.collection('organisations').save(organisation, function (err) {
                if (err) callback(err);
                callback(null, organisation);
            })
        }
    }
}

module.exports = organisationService;
