var ObjectID = require('mongodb').ObjectID;

var service = function (mongodb) {

    return {

        findById: function (id, callback) {
            var query = {_id: ObjectID(id)}
            mongodb.collection('tokens').findOne(query, function (err, data) {
                if (err) return callback(err);
                callback(null, data);
            })
        },

        findByCode: function (code, callback) {
            var query = {code: code}
            mongodb.collection('tokens').findOne(query, function (err, data) {
                if (err) return callback(err);
                callback(null, data);
            })
        },

        save: function (data, callback) {
            mongodb.collection('tokens').save(data, function (err) {
                if (err) callback(err);
                callback(null, data);
            })
        }

    }
};

module.exports = service;
