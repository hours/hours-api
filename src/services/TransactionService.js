var transactionService = function (mongodb) {

    return {
        findBalance: function (organisation, callback) {
            var query = {
                organisation: organisation,
                $or: [
                    {status: "paid"},
                    {status: {$exists: false}}
                ]
            };

            mongodb.collection('transactions').find(query).toArray(function (err, transactions) {
                if (err) return callback(err);
                var total = 0;
                transactions.forEach(function (transaction) {
                    total += transaction.amount;
                })
                callback(null, total);
            });
        }
    }
}

module.exports = transactionService;
