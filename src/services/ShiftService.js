var ObjectID = require('mongodb').ObjectID;

var service = function (mongodb) {

    var collection = mongodb.collection('shifts');

    return {

        findById: function (id, callback) {
            var query = {_id: ObjectID(id)}
            collection.findOne(query, function (err, data) {
                if (err) return callback(err);
                return callback(null, data);
            })
        },

        save: function (shift, callback) {
            collection.save(shift, function (err) {
                if (err) return callback(err);
                return callback(null, shift);
            });
        },

        remove: function (shift, callback) {

            if(!shift._id)
                return callback('CANNOT_DELETE_WHEN_ITEM_HAS_NO_ID')

            var query = {_id: ObjectID(shift._id)};
            collection.removeOne(query, function (err, shift) {
                if (err) return callback(err);
                return callback(null, shift);
            });
        }

    };

};

module.exports = service;
