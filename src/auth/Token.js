var express = require('express');

var moment = require('moment');
var jwt = require('jwt-simple');

var ObjectID = require('mongodb').ObjectID;


module.exports = function (mongodb, config) {

    var redirectUtil = require('../util/RedirectUtil.js');

    var userService = require('../services/UserService.js')(mongodb);

    var router = express();

    router.get('/token', function (req, res, next) {

        var userId = req.context.user || req.user;

        userService.findById(userId, function (err, user) {

            if (err) return next(err);

            // create token
            var expires = moment().add(7, 'days').valueOf();

            var payload = {
                iss: {
                    _id: user._id,
                    name: user.name
                },
                exp: expires
            };

            var token = jwt.encode(payload, config.jwtTokenSecret);
            res.set('x-token', token);

            var redirectUrl = redirectUtil(config).url(req)
            res.redirect(redirectUrl + '#access_token=' + token);

        });

    });

    return router;

};
