var express = require('express');
var bodyParser = require('body-parser');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

var crypto = require('crypto');

module.exports = function (mongodb) {

    passport.use(new LocalStrategy(
        function (username, password, done) {

            var regex = new RegExp('^' + username.trim() + '$', 'i')

            var collection = mongodb.collection('users');
            var query = {
                "username": regex
            };

            collection.findOne(query, function (err, user) {
                if (err) done(err);
                if (!user) {
                    return done(null, false, {message: 'Incorrect username.'});
                }
                var hash = crypto.createHash('sha1').update(password).digest('hex');
                if (user.password !== hash) {
                    return done(null, false, {message: 'Incorrect password.'});
                }
                user.name = user.firstName + " " + user.lastName;
                return done(null, user);
            });
        }
    ));


    var router = express.Router();

    router.use("/login", bodyParser.urlencoded({extended: false}));

    router.post('/login',
        passport.authenticate('local', {
            successReturnToOrRedirect: '/token',
            failureRedirect: '/login.html#?error',
            failureFlash: true
        })
    );

    router.get('/local', function (req, res) {
        var redirectUrl = "/login.html"
        res.redirect(redirectUrl);
    });

    router.get('/signup', function (req, res) {
        var redirectUrl = "/signup.html"
        res.redirect(redirectUrl);
    });

    return router;

}
