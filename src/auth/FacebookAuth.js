var express = require('express');

var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;

module.exports = function (mongodb, config) {

    passport.use(new FacebookStrategy({
                clientID: config.auth.facebook.clientID,
                clientSecret: config.auth.facebook.clientSecret,
                callbackURL: config.auth.facebook.callbackURL
            },
            function (accessToken, refreshToken, profile, done) {
                process.nextTick(function () {
                    var user = {
                        "ref": profile.id,
                        "name": profile.displayName,
                        "provider": "facebook",
                        "picture": "http://graph.facebook.com/" + profile.id + "/picture"
                    };

                    var collection = mongodb.collection('users');
                    var query = {
                        "ref": user.ref,
                        "provider": user.provider
                    };

                    collection.findOne(query, function (err, item) {
                        if (err) callback(err);
                        if (item) {
                            done(null, item)
                        } else {
                            collection.insertOne(user, function (err) {
                                if (err) callback(err);
                                done(null, item)
                            });
                        }
                    });
                });
            }
        ));


    var router = express.Router();

    router.get('/facebook', passport.authenticate('facebook'), function (req, res) {});

    router.get('/facebook/callback', passport.authenticate('facebook', {
        successReturnToOrRedirect: '/token',
        failureRedirect: '/login.html#?error',
        failureFlash: true
    }));

    return router;

}
