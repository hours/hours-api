var express = require('express');
var bodyParser = require('body-parser');

var uuid = require('uuid');
var oauth2orize = require('oauth2orize');
var moment = require('moment');

var jwt = require('jwt-simple');
var passport = require('passport');

var BasicStrategy = require('passport-http').BasicStrategy;
var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;

var login = require('connect-ensure-login');

module.exports = function (mongodb, config) {


    var userService = require('../services/UserService.js')(mongodb);
    var tokenService = require('../services/TokenService.js')(mongodb);
    var clientService = require('../services/ClientService.js')(mongodb);

    var router = express.Router();

    passport.use('oauth-basic', new BasicStrategy(
        function (username, password, done) {
            clientService.findById(username, function (err, client) {
                if (err) return done(err);
                if (!client) return done(null, false);
                if (client.secret != password) return done(null, false);
                return done(null, client);
            });
        }
    ));

    passport.use(new ClientPasswordStrategy(
        function (clientId, clientSecret, done) {
            clientService.findById(username, function (err, client) {
                if (err) return done(err);
                if (!client) return done(null, false);
                if (client.secret != clientSecret) return done(null, false);
                return done(null, client);
            });
        }
    ));

    var server = oauth2orize.createServer();

    server.serializeClient(function (client, done) {
        return done(null, client._id);
    });

    server.deserializeClient(function (id, done) {
        clientService.findById(id, function (err, client) {
            if (err) {
                return done(err);
            }
            return done(null, client);
        });
    });

    server.grant(oauth2orize.grant.code(function (client, redirectUri, user, ares, done) {
        var code = uuid.v1();

        var data = {
            code: code,
            client: client._id,
            user: user,
            scope: ares.scope,
            redirectUri: redirectUri
        };

        tokenService.save(data, function (err) {
            if (err) return done(err);
            return done(null, code);
        });
    }));

    server.exchange(oauth2orize.exchange.code(
        function (client, code, redirectUri, done) {
            tokenService.findByCode(code, function (err, access) {
                if (err) return done(err);
                if (client._id.toString() !== access.client.toString()) return done(null, false);
                if (redirectUri !== access.redirectUri) return done(null, false);
                userService.findById(access.user, function (err, user) {
                    if (err) return done(err);

                    var expires = moment().add(7, 'days').valueOf();
                    var payload = {
                        iss: user,
                        exp: expires
                    };

                    var token = jwt.encode(payload, config.jwtTokenSecret);
                    return done(null, token);
                });
            });
        })
    );

    router.use( bodyParser.urlencoded({extended: false}));

    router.get('/authorize',
        login.ensureLoggedIn('/'),
        server.authorize(
            function (clientId, redirectUri, done) {
                clientService.findById(clientId, function (err, client) {
                    if (err) return done(err);
                    if (!client) return done(null, false);
                    if (client.redirectUri !== redirectUri) return done(null, false);
                    return done(null, client, client.redirectUri);
                });
            }),
        function (req, res) {
            var form = [
                '<p>Hi' + req.oauth2.user.name + '!</p>',
                '<p><b>' + req.oauth2.client.name + '</b> is requesting access to your account.</p>',
                '<p>Do you approve?</p>',

                '<form action="authorize" method="post">',
                '<input name="transaction_id" type="hidden" value="' + req.oauth2.transactionID + '">',
                '<div>',
                '<input type="submit" value="Allow" id="allow">',
                '<input type="submit" value="Deny" name="cancel" id="deny">',
                '</div>',
                '</form>'
            ];
            res.set('transactionID', req.oauth2.transactionID);
            res.send(form.join(''));
        });

    router.post('/authorize',
        login.ensureLoggedIn('/'),
        server.decision());


    router.post('/token',
        passport.authenticate(['oauth-basic', 'oauth2-client-password'], { session: false }),
        server.token(),
        server.errorHandler());

    return router;

};
