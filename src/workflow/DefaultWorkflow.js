var moment = require('moment');

var userUtil = require('../util/UserUtil.js');
var statusUtil = require('../util/StatusUtil.js');

module.exports = function (mongodb) {

    var shiftService = require('../services/ShiftService.js')(mongodb);
    var organisationService = require('../services/OrganisationService.js')(mongodb);

    var organisationUtil = require('../util/OrganisationUtil.js');

    return {

        create: function(req, shift, organisation, callback){
            var user = organisationUtil.findUserInOrganisation(req.user, organisation);

            var data = {
                type: shift.type,

                user: shift.user,
                organisation: organisation._id.toString(),

                project: shift.project,
                category: shift.category,

                status: shift.status
            };

            if (shift.type === 'SHIFT') {
                data.pause = shift.pause;

                var from = moment(shift.from);
                var to = moment(shift.to);

                data.from = from.toDate();
                data.to = to.toDate();

                // Set to data to next day when from after to
                if (from.isAfter(to)) {
                    var diff = from.diff(to, 'days')
                    data.to = to.add(diff + 1, 'day').toDate();
                }
            }

            if (shift.type === 'BLOCKED') {

                var from = moment(shift.from);
                var to = moment(shift.to);

                data.from = from.toDate();
                data.to = to.toDate();

            }

            if (shift.type === 'EXPENSE') {
                data.date = moment(shift.date).toDate();
                data.description = shift.description;
                data.amount = shift.amount;
                data.picture = shift.picture;
            }

            if (shift.type === 'MILEAGE') {
                data.date = moment(shift.date).toDate();
                data.distance = shift.distance;
                data.origin = shift.origin;
                data.destination = shift.destination;
            }

            // Set init user
            data.user = userUtil.init(req, user, data);

            // Set init status
            data.status = statusUtil.init(req, user, data);

            // User has no role in organisation
            if (!user)
                return callback("USER_HAS_NO_ROLE_IN_ORGANISATION");

            // Set user has no role in organisation
            if (user.role !== 'owner' && shift.user)
                return callback("NOT_ALLOWED_TO_ADD_SHIFT_FOR_OTHER_USER");

            // Only owner can set status approved
            if (user.role !== 'owner' && shift.status === 'approved')
                return callback("NOT_ALLOWED_TO_SET_STATUS_APPROVED");

            // Set user has no role in organisation
            if (data.user !== null && !organisationUtil.findUserInOrganisation(data.user, organisation))
                return callback("SET_USER_HAS_NO_ROLE_IN_ORGANISATION");

            callback(null, data);
        },

        update: function (req, shift, organisation, data, callback) {
            var user = organisationUtil.findUserInOrganisation(req.user, organisation)
            if (!user) return callback("USER_HAS_NO_ROLE_IN_ORGANISATION");

            data.project = shift.project;
            data.category = shift.category;

            data.pause = shift.pause;
            data.amount = shift.amount;
            data.description = shift.description;
            data.comment = shift.comment;
            data.picture = shift.picture;

            data.distance = shift.distance;
            data.origin = shift.origin;
            data.destination = shift.destination;

            data.type = shift.type;

            if (shift.date) {
                data.date = moment(shift.date).toDate();
            }

            if (shift.from && shift.to) {

                var from = moment(shift.from);
                var to = moment(shift.to);

                data.from = moment(shift.from).toDate();
                data.to = moment(shift.to).toDate();

                if (from.isAfter(to)) {
                    var diff = from.diff(to, 'days')
                    data.to = to.add(diff + 1, 'day').toDate();
                }
            }

            if (user.role === "owner") {

                if (shift.user && !organisationUtil.findUserInOrganisation(shift.user, organisation))
                    return callback("Set user has no role in organisation");

                if (shift.user || shift.user === null)
                    data.user = shift.user;

                if (!shift.user && shift.user !== null)
                    data.user = req.user;

                if (data.user === null)
                    shift.status = "open";

                if (data.user === req.user && !shift.status)
                    shift.status = "approved";

                data.status = shift.status;

            } else {
                if (!shift.status || shift.status === null)
                    shift.status = "pending";

                if (shift.status === "blocked" || shift.status === "pending")
                    data.status = shift.status;

                if (shift.status === "rejected" || shift.status === "planned")
                    data.status = "pending";
            }

            callback(null, data);
        },

        remove: function (req, shift, callback) {

            if (!shift)
                return callback("SHIFT_NOT_FOUND_IN_DATABASE");

            organisationService.findById(shift.organisation, function (err, organisation) {
                if (err) return next(err);

                var user = organisationUtil.findUserInOrganisation(req.user, organisation);

                if (!user) return callback("USER_HAS_NO_ROLE_IN_THIS_ORGANISATION");

                if (user.id != shift.user && user.role === "worker")
                    return callback("USER_IS_NOT_ALLOWED_TO_REMOVE_SHIFT");

                // If status assigned and status to open
                if (shift.status === "assigned" && user.role !== "owner" && user.id === shift.user) {
                    shift.user = null;
                    shift.status = "open";
                    shiftService.save(shift, function (err, data) {
                        if (err) return next(err);
                        return callback(null, data);
                    });
                    return;
                }

                shift.status = 'removed'
                shiftService.save(shift, function (err, data) {
                    if (err) return next(err);

                    return callback(null, data);
                });


            });
        },

        approve: function (req, shift, callback) {
            organisationService.findById(shift.organisation, function (err, organisation) {
                if (err) return next(err);

                var user = organisationUtil.findUserInOrganisation(req.user, organisation)

                if (!user)
                    return callback("USER_HAS_NO_ROLE_IN_THIS_ORGANISATION");

                if (shift.status !== 'pending')
                    return callback("CAN_ONLY_APPROVE_WHEN_STATUS_IS_PENDING");

                if (user.role !== "owner" && user.role !== "approver")
                    return callback("USER_IS_NOT_ALLOWED_TO_APPROVE_HOURS_FOR_THIS_ORGANISATION");

                shift.status = "approved";
                callback(null, shift);

            });
        },

        reject: function (req, shift, callback) {
            organisationService.findById(shift.organisation, function (err, organisation) {
                if (err) return next(err);

                var user = organisationUtil.findUserInOrganisation(req.user, organisation);

                if (!user)
                    return callback("USER_HAS_NO_ROLE_IN_THIS_ORGANISATION");

                if (shift.status !== 'pending')
                    return callback("CAN_ONLY_REJECT_WHEN_STATUS_IS_PENDING");

                if (user.role !== "owner" && user.role !== "approver")
                    return callback("USER_IS_NOT_ALLOWED_TO_APPROVE_HOURS_FOR_THIS_ORGANISATION");

                shift.status = "rejected";
                callback(null, shift);

            });
        }
    }

};
